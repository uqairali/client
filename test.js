import React from "react";
import "./styles.css";
import { jsPDF } from "jspdf";
import html2canvas from 'html2canvas'
export default function App() {
  const printDocument=()=> {
    const input = document.getElementById('divToPrint');
    html2canvas(input)
      .then((canvas) => {
        const imgData = canvas.toDataURL('image/png');
        var imgWidth = 210; 
        var pageHeight = 295;  
        var imgHeight = canvas.height * imgWidth / canvas.width;
        var heightLeft = imgHeight;
        var doc = new jsPDF('p', 'mm');
        var position = 10; // give some top padding to first page
        
        doc.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight);
        heightLeft -= pageHeight;
        
        while (heightLeft >= 0) {
          position += heightLeft - imgHeight; // top padding for other pages
          doc.addPage();
          doc.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight);
          heightLeft -= pageHeight;
        }
        doc.save( 'file.pdf');
      })
    ;
  }
  return (
    <div>
    <div className="mb5">
      <button onClick={printDocument}>Print</button>
    </div>
    <div id="divToPrint" className="mt4" style={{
      width: '210mm',
      minHeight: '297mm',
      marginLeft: 'auto',
      margin: '10px',
      color:'red'
    }}>
     {
       [1,2,3,4,5,6,7,6,6,5,5,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,].map((itm,key)=>(
<h1 >میں ٹھیک ہوں{key}</h1>
       ))
     }

      <div>Note: Here the dimensions of div are same as A4</div> 
      <div>You Can add any component here</div>
    </div>
  </div>
  );
}
