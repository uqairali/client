/*!

=========================================================
* Light Bootstrap Dashboard PRO React - v1.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/light-bootstrap-dashboard-pro-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";
// this is used to create scrollbars on windows devices like the ones from apple devices
import PerfectScrollbar from "perfect-scrollbar";
import "perfect-scrollbar/css/perfect-scrollbar.css";
// react component that creates notifications (like some alerts with messages)
import NotificationSystem from "react-notification-system";

import Sidebar from "components/Sidebar/Sidebar.jsx";
import AdminNavbar from "components/Navbars/AdminNavbar.jsx";
import Footer from "components/Footer/Footer.jsx";

import image from "assets/img/full-screen-image-3.jpg";
import Loading from '../components/loader'
// dinamically create dashboard routes
import routes from "routes.js";
import { connect } from 'react-redux'
// style for notifications
import { style } from "variables/Variables.jsx";
import SetTitle from 'utils/appTitle'
import { onGetClasses,onGetInstitute } from '../store/Actions/mainActions'
var ps;

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      _notificationSystem: null,
      image: image,
      color: "black",
      hasImage: true,
      navbar: false,
      mini: true,
      fixedClasses: "dropdown show-dropdown open"
    };
   
  }


  componentDidMount() {
    this.setState({ _notificationSystem: this.refs.notificationSystem });
    if (navigator.platform.indexOf("Win") > -1) {
      ps = new PerfectScrollbar(this.refs.mainPanel);
    }
  }
  componentWillUnmount() {
    if (navigator.platform.indexOf("Win") > -1) {
      ps.destroy();
    }
  }
  componentDidUpdate(e) {
    if (navigator.platform.indexOf("Win") > -1) {
      setTimeout(() => {
        ps.update();
      }, 350);
    }
    if (e.history.action === "PUSH") {
      document.documentElement.scrollTop = 0;
      document.scrollingElement.scrollTop = 0;
      this.refs.mainPanel.scrollTop = 0;
    }
    if (
      window.innerWidth < 993 &&
      e.history.action === "PUSH" &&
      document.documentElement.className.indexOf("nav-open") !== -1
    ) {
      document.documentElement.classList.toggle("nav-open");
    }
  }
  componentWillMount() {
    if (document.documentElement.className.indexOf("nav-open") !== -1) {
      document.documentElement.classList.toggle("nav-open");
    }
  }
  // function that shows/hides notifications - it was put here, because the wrapper div has to be outside the main-panel class div
  handleNotificationClick(type, message) {
    if (type === "errorModal") {
      this.setState({ showErrorModalMessage: message, showErrorModal: true })
      return
    }
    let position = "tc";
    //var level;

    let level;
    let icon;
    switch (type) {
      case 'success':
        level = 'success';
        icon = 'fa fa-check';
        break;
      case 'warning':
        level = 'warning';
        icon = 'fa fa-exclamation-triangle';
        break;
      case 'error':
        level = 'error';
        icon = 'fa fa-times';
        break;
      case 'info':
        level = 'info';
        icon = 'fa fa-info';
        break;
      default:
        level = 'success';
        icon = 'fa fa-check';
        break;
    }
    this.state._notificationSystem.addNotification({
      title: <span data-notify="icon" className={icon} />,
      message: (
        <div className='wb'>
          {message}
        </div>
      ),
      level: level,
      position: position,
      autoDismiss: 3
    });
  }
  handleImageClick = image => {
    this.setState({ image: image });
  };
  handleColorClick = color => {
    this.setState({ color: color });
  };
  handleHasImage = hasImage => {
    this.setState({ hasImage: hasImage });
  };
  handleNavbarClick = navbar => {
    this.setState({ navbar: navbar });
  };
  handleMiniClick = () => {
    this.setState({ mini: !this.state.mini });
    document.body.classList.toggle("sidebar-mini");
  };
  handleFixedClick = () => {
    if (this.state.fixedClasses === "dropdown") {
      this.setState({ fixedClasses: "dropdown show-dropdown open" });
    } else {
      this.setState({ fixedClasses: "dropdown" });
    }
  };
  getRoutes = routes => {
    return routes.map((prop, key) => {
    
      if (prop.collapse) {
        return this.getRoutes(prop.views);
      }
      if (prop.layout === "/admin") {
        return (
          <Route
            path={prop.layout + prop.path}
            key={key}
            render={routeProps => (
              <prop.component
                {...routeProps}
                handleClick={this.handleNotificationClick}
              />
            )}
          />
        );
      } else {
        return null;
      }
    });
  };
  render() {
    this.props.catchError&&
    this.props.onClearCatchError()
    return (
      <div className="wrapper">
         {/* set app title dinamically by route name */}
        <SetTitle location={this.props} />
        {
          this.props.loading&&
          <Loading/>
        }
        <NotificationSystem ref="notificationSystem" style={style} />
        {
          this.props.catchError ?
            this.handleNotificationClick(this.props.catchErrorType, this.props.catchError)
            : null
        }
        <Sidebar
          {...this.props}
          image={this.state.image}
          color={this.state.color}
          hasImage={this.state.hasImage}
          mini={this.state.mini}
        />
        <div
          className={
            "main-panel" +
            (this.props.location.pathname === "/maps/full-screen-maps"
              ? " main-panel-maps"
              : "")
          }
          ref="mainPanel"
        >
          <AdminNavbar
            {...this.props}
            handleMiniClick={this.handleMiniClick}
            navbar={this.state.navbar}
          />
          <Switch>{this.getRoutes(routes)}</Switch>
          <Footer fluid />
         
        </div>
      </div>
    );
  }
}

const mapStateToProps=state=>{
  return{
    catchError: state.notificationMessage,
    catchErrorType: state.notificationType,
    loading:state.loading,
  }
}
const mapDispatchToProps=dispatch=>{
  return{
    onClearCatchError: () => dispatch({ type: 'CLEAR_NOTIFICATION' }),
    onShowNotification: (errorType, error) => dispatch({ type: "SET_NOTIFICATION", errorMessage: error, errorType: errorType })
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(Dashboard);
