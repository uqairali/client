import moment from 'moment'
import { useSpeechSynthesis } from 'react-speech-kit';

export const dateFormate = (date) => {
  return moment(date).format("MMM Do YYYY");
}
export const dateFormate2 = (date) => {
  return moment(date).format("DD/MM/YYYY - HH:mm");
}
export const DateOnlyTime = (date) => {
  return moment(date).format("HH:mm:ss");
}
export const paramsDateFormet = (date) => {
  return moment(date).format("MM-DD-YYYY");
}
export const CustomDateFormate = (date,formate) => {
  return moment(date).format(formate);
}
export const countDiscount = (discount, total) => {
  return total - (total * discount) / 100
}

export const getFilterDays = days => {
  return {
    from: moment()
      .add(days, "days")
      .toDate(),
    to: new Date()
  };
};

export function getThisMonthFilter() {
  var result = [];
  var startDate = new Date();
  var endDate = new Date();
  if (startDate.getDate() > 25) {
    startDate.setDate(26);
  } else {
    startDate.setMonth(startDate.getMonth() - 1);
    startDate.setDate(26);
  }
  if (startDate.getMonth() === 11) {
    endDate.setMonth(0);
    endDate.setFullYear(startDate.getFullYear() + 1);
  } else {
    endDate.setMonth(startDate.getMonth() + 1);
  }

  endDate.setDate(25);

  result[0] = startDate.setHours(0, 0);
  // result[1] = new Date();
  result[1] = endDate.setHours(23, 59);
  result[2] = endDate.setHours(23, 59);

  return result;
}

export function getLastMonthFilter() {
  var result = getThisMonthFilter();
  var startDate = new Date(result[0]);
  var endDate = new Date(result[2]);
  startDate.setMonth(startDate.getMonth() - 1);
  endDate.setMonth(endDate.getMonth() - 1);
  endDate.setDate(25);
  result[0] = startDate.setHours(0, 0);
  result[1] = endDate.setHours(23, 59);
  result[2] = endDate.setHours(23, 59);
  return result;
}

export const formatDateTimeRange = (startDate, endDate) => {
  startDate = moment(startDate);
  endDate = moment(endDate);

  let dayDifferenceCurrentDate = moment().diff(startDate, "days");
  let dayDiffernce = endDate.diff(startDate, "days");
  //var mins = moment.utc(moment(endDate, "HH:mm:ss").diff(moment(startDate, "HH:mm:ss"))).format("HH:mm")
  var ms = Math.abs(
    moment(startDate, "DD/MM/YYYY HH:mm:ss").diff(
      moment(endDate, "DD/MM/YYYY HH:mm:ss")
    )
  );
  var d = moment.duration(ms);
  var s = Math.floor(d.asHours()) + moment.utc(ms).format(":mm");

  if (isLastWeek(startDate, endDate)) {
    return "Last Week";
  }
  if (isThisMonth(startDate, endDate)) {
    return "This Month";
  }
  if (isLastMonth(startDate, endDate)) {
    return "Last Month";
  }

  if (
    dayDiffernce === 6 &&
    startDate.weekday() === 0 &&
    endDate.weekday() === 6 &&
    moment().isBetween(startDate, endDate)
  ) {
    return "This Week";
  }
  if (
    dayDifferenceCurrentDate === 1 &&
    dayDiffernce === 1 &&
    parseFloat(s) <= 24.0
  ) {
    return s + " Hours";
  }
  switch (dayDifferenceCurrentDate) {
    case 0:
      return "Today";
    case 1:
      return "Yesterday";
    case 3:
      return "Last 3 Days";
    case 7:
      return "Last 7 Days";
    case 30:
      return "Last 30 Days";
    default: {
      return `${formatDate(startDate, dateFormats.f1)} - ${formatDate(
        endDate,
        dateFormats.f1
      )}`;
    }
  }
};
function isLastWeek(startDate, endDate) {
  return (
    compareDate(startDate, GetLastWeekStart()) &&
    compareDate(endDate, GetLastWeekEnd())
  );
}
function isThisMonth(startDate, endDate) {
  var date = new Date(),
    y = date.getFullYear(),
    m = date.getMonth();
  var firstDay = new Date(y, m, 1);
  var lastDay = new Date(y, m + 1, 0);

  firstDay = moment(firstDay);
  lastDay = moment(lastDay);
  return compareDate(startDate, firstDay) && compareDate(endDate, lastDay);
}
function isLastMonth(startDate, endDate) {
  let lastMonthStartDate = moment()
    .subtract(1, "months")
    .startOf("month");
  let lastMonthEndDate = moment()
    .subtract(1, "months")
    .endOf("month");
  return (
    compareDate(startDate, lastMonthStartDate) &&
    compareDate(endDate, lastMonthEndDate)
  );
}
export const formatDate = (date, format) => {
  return moment(new Date(date)).format(format ? format : "MMM Do YY, h:mm a");
};
export const dateFormats = {
  f1: "MMM Do, YY" //Do MMM, YYYY"
};
function compareDate(d1, d2) {
  return formatDate(d1, dateFormats.f1) === formatDate(d2, dateFormats.f1);
}
function GetLastWeekStart() {
  var today = moment();
  var daystoLastSunday = 0 - (1 - today.isoWeekday()) + 8;

  var lastSunday = today.subtract("days", daystoLastSunday);

  return lastSunday;
}
function GetLastWeekEnd() {
  var lastSunday = GetLastWeekStart();
  var lastSaturday = lastSunday.add("days", 6);

  return lastSaturday;
}

//function to sort the results
export const filterCaseInsensitive = (filter, row) => {
  const id = filter.pivotId || filter.id;
  var searchString = (typeof row[filter.id]) === 'number' ? row[filter.id].toString() : row[filter.id]
  return (
    row[id] !== undefined ?
      filter.value === "Paid" || filter.value === "unPaid" ?
        (searchString.toUpperCase().startsWith(filter.value.toUpperCase()))
        : (searchString.toUpperCase().includes(filter.value.toUpperCase()))
      :
      true
  );
}

//get this month start and end date
export const findStartAndEndDAte = (date) => {
  var monthStartDay = new Date(moment(date).startOf('month'))
  var monthEndDay = new Date(moment(date).endOf('month'))
  return { start: monthStartDay, end: monthEndDay }
}

export const PrintFeeRowCol = (data) => {
  const row = ["reg_Number", "student_name", "class", "monthlyFee", "deposit","deposit_date", "total", "balance"],
        column = []
  data.map(itm => {
    column.push({
      reg_Number: itm.student.registrationNumber,
      student_name: itm.student.name,
      class: itm.instituteClass && itm.instituteClass.name,
      monthlyFee: itm.monthlyFee,
      //discount: itm.discount + "%",
      //discountAmount: itm.discountAmount,
      deposit: itm.deposit,
      total: itm.total,
      balance: itm.total - itm.deposit,
      deposit_date:itm.submissionDate?dateFormate2(itm.submissionDate):"-",
     // status: itm.deposit ? "paid" : "unpaid"
    })
  })
  return {row,column}
}
export const capitalize = (s) => {
  if (typeof s !== 'string') return ''
  return s.charAt(0).toUpperCase() + s.slice(1)
}
export const calCulateTotalAmmount=(data)=>{
  var newCalculateArray=[]
  for(var i=0;i<=data.length-1;i++){
      if(data[i].isPayed){
         if(newCalculateArray.length>=1){
             newCalculateArray.push(newCalculateArray[i-1]-data[i].payment)
         }else{
            newCalculateArray.push(-data[i].payment)
         }
      }else{
        if(newCalculateArray.length>=1){
            newCalculateArray.push(newCalculateArray[i-1]+data[i].payment)
        }else{
           newCalculateArray.push(data[i].payment)
        }
      }
  }
  return newCalculateArray
}
export const calCulateAmmountByAddress=(arr)=>{
  var total=0
  arr.map(cnt=>{
    total+=cnt.ammount
  })
  return total
}
export const onCalculateSubTotal=(arr)=>{
  var total=0
  arr.map(val=>{
    total+=val.subTotal  })
  return total
}
export const calCulateTotalReciptAmmount=(arr)=>{
  var total=0;
  arr.map(val=>{
    total+=val.totalAmmount
  })
  return total
}
export const calculatePayment=(arr)=>{
  var total=0;
  arr.map(val=>{
    total+=val.ammount
  })
  return total
}


export const startAndEndOfTheMonth=(date=new Date())=>{
  const startOfMonth = moment(date).startOf('month');
  const endOfMonth = moment(date).endOf('month');
  return {startOfMonth,endOfMonth}
}
