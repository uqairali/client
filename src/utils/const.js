export const colors =(index)=>{
    var colorIndex=["#f04732", "#2096f3", "#5cb85c", "#fec431", "#7c57b6", "#f26432", "#eb4e63", "#48c7da", "#c0ca33", "#f8a031", "#7b283c", "#063376", "#bd372f", "#0e5ccc", "#468847", "#f79531", "#795548", "#c09853", "#288eb1", "#8c8525"]
     var key=index>19?(index-20):index
     return colorIndex[key]
    }

    export const AppName="Shop"

    export const PaidStatus=[
        {label:"Paid",value:"Paid"},
        {label:"unPaid",value:"unPaid"}
    ]

    export const icons={
        view:'pe-7s-look',
        edit:'pe-7s-pen',
        delete:'pe-7s-trash',
        submit:'pe-7s-smile',
        cart:'pe-7s-cart',
        print:'pe-7s-print',
        unlock:'pe-7s-unlock',
        lock:'pe-7s-lock'

    }