
import Dashboard from "views/Dashboard.jsx";
import Login from "views/Login.jsx";
import UnAuthorized from "views/UnAuthorized.jsx";

//************************my routes*******************************
import Account from 'views/account/account'
import Addreses from 'views/Address/address'
import Items from 'views/items/items'
import Book from 'views/book/book'
import Kata from 'views/kata/kata'
import Recipts from 'views/recipts/recipts'
import payments from "views/payments/payments";
import Reports from 'views/reports/report'
import Customer from 'views/customer';

var dashboardRoutes = [
  //********************my routes ***********************/
  {
    path: "/dashboard",
    layout: "/admin",
    name: "ڈیش  بورڈ",
    icon: "pe-7s-glasses",
    component: Dashboard
  },
  {
    path: "/items",
    layout: "/admin",
    name: "سٹاک  سیٹنگ",
    icon: "pe-7s-config",
    component: Items
  },
  {
    path: "/account",
    layout: "/admin",
    name: "اکاونٹ",
    icon: "pe-7s-users",
    component: Account
  },
  {
    path: "/addreses",
    layout: "/admin",
    name: "گاو‏ں",
    icon: "pe-7s-home",
    component: Addreses
  },

  {
    path: "/eBook",
    layout: "/admin",
    name: "روزنامچہ",
    icon: "pe-7s-note",
    component: Book
  },
  {
    path: "/recipts",
    layout: "/admin",
    name: "رسیدے",
    icon: "pe-7s-search",
    component: Recipts
  },
  {
    path: "/payments",
    layout: "/admin",
    name: "وصولی",
    icon: "pe-7s-smile",
    component: payments
  },
  {
    path: "/customers",
    layout: "/admin",
    name: "تلاش کریں",
    icon: "pe-7s-user",
    component: Customer
  },
  {
    path: "/reports",
    layout: "/admin",
    name: "رپورٹ",
    icon: "pe-7s-file",
    component: Reports
  },
  {
    path: "/kata/:id/:reciptId?",
    layout: "/admin",
    name: "کاتہ",
    icon: "pe-7s-calculator",
    component: Kata,
    isHideSideIcon:true
  },
];

export const accountRoutes = [
  {
    path: "/login",
    name: "Login",
    icon: "pe-7s-graph",
    component: Login,
    layout: "/account"
  },
  {
    path: "/unauthorized",
    name: "Aunthorized",
    icon: "pe-7s-graph",
    component: UnAuthorized,
    layout: "/account"
  },

];
export default dashboardRoutes;
