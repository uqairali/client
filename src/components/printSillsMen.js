import React, { useState, useEffect, Fragment } from 'react'
import Button from "components/CustomButton/CustomButton.jsx";
import { calCulateTotalAmmount } from 'utils/helper'
const qz = require("qz-tray");
const NodeThermalPrinter = ({ data }) => {
    const [calculateAmmont, setCalculateAmmount] = useState([])
    useEffect(() => {
        setCalculateAmmount(calCulateTotalAmmount(data))

    }, [data])
    const printData = () => {
        return [
            {
                type: 'pixel',
                format: 'html',
                flavor: 'plain',
                data: `<html>
                <body>
               
<h4 style="text-align: center;height:1%;">السیدجنرل سٹور گل مارکیٹ بٹگرام</h4>
<h5 style="text-align: center;height:1%"> نام: ${data.length > 0 ? data[0].name.name : '-'}</h5>
                <table style="width:100%;text-align:left;font-size:14px;border:1px solid black">
            <tr>
            <th>تاریح</th>
            <th style="text-align:center">تفسیل</th>
            <th>رقم</th>
                <th>ٹوٹل  رقم</th>
            </tr>
            ${
                    data.map((element, key) => {



                        return `<tr>
                        <td style="text-align:left;font-weight:bold">${element.date}</span></td>
                        <td style="text-align: center;font-weight:bold">${element.isPayed ? "وصول" : "بل"}</td>
                        <td style="font-weight:bold">${element.payment}</td>
                <td style="font-weight:bold">${calculateAmmont[key]}</td>
            </tr>`

                    })

                    }
            
        </table>
                
                <h5 style="text-align:center">${data.length > 0 ? data[0].name.ammount : 0} -------------------ٹوٹل</h5>
                      <p style="text-align:left;font-family: cursive;font-size:8px;font-weight: bold;">Developed by:Uqair Ali</p>
                <br/>
              
                  </body>
                  </html>`

            }
        ];
    }
    const onGoPrint = async () => {
        try {



            const websocket = await qz.websocket.connect({ retries: 0, delay: 1 })
            let config = await qz.configs.create("BC-95AC")
            console.log("testc", config)

            printData().push(); //cut paper


            await qz.print(config, printData()).then(pr=>{
            }).catch(err=>{
                alert("err",err)
            })

            await qz.websocket.disconnect()
        }
        catch (err) {
            console.log(err)
        }
    }
    const onPrint = async () => {
        try {
            onGoPrint()
        } catch (err) {
            console.log(err)
        }

    }

    return (
        <div>
            <Button
                disabled={!data.length}
                onClick={onPrint}
                className="btn-submit" fill type="submit"><i className={"fa fa-print"} />
                {"Print"}
            </Button>
        </div>
    )
}

export default NodeThermalPrinter