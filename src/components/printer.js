import React, { useState, useEffect, Fragment } from 'react'
import Button from "components/CustomButton/CustomButton.jsx";
const qz = require("qz-tray");
const NodeThermalPrinter = ({ remining, payment, setIspendingPrint, isPendingPrint, serialNumber, receiptArray, date, clientName, allItems, onBeforePrint, onSuccessPrint }) => {
    const [isConnect, setIsConnect] = useState(false)
    var subTotal = 0;
    var total=0;
    receiptArray.map(val => {
        subTotal += val.subTotal
        total+=val.subTotal
    })
    if(remining){
        total+=+remining
    }
    if(payment){
        total=total-payment
    }
    const printData = () => {
        return [
            {
                type: 'pixel',
                format: 'html',
                flavor: 'plain',
                data: `<html>
                <body>
                <h5 style="text-align:center">03125310579 ------------ رابطہ  نمبر</h5>
                <table style="width: 100%;">
<tr>
    <td>
        ${date} :تاریخ
    </td>
    <td style="text-align: right;">
        ${serialNumber} :بل نمبر
    </td>
</tr>
</table>
<h4 style="text-align: center;height:1%;">السید  نؤجنرل  سٹور  پیرھاڑی</h4>
<h5 style="text-align: center;height:1%">سیدقاسم علی شاہ -- 03439545645</h5>
<h5 style="text-align: center;height:1%">سیدجماعت  علی شاہ -- 03125310579</h5>
<h5 style="text-align: center;height:1%">سید  ملک شاہ ------ 03018123767</h5>

<h5 style="text-align: center;height:1%"> نام: ${clientName}</h5>
                <table style="width:100%;text-align:left;font-size:14px;border:1px solid black">
            <tr>
                <th>روپے</th>
                <th>نرخ</th>
                <th style="text-align:center">تفسیل</th>
                <th>تعداد</th>
            </tr>
            ${
                    receiptArray.map((element, key) => {
                        
                        
                            return `<tr>
                <td style="font-weight:bold">${element.subTotal}</td>
                <td style="font-weight:bold">${element.price}</td>
                <td style="text-align: right;font-weight:bold">${element.item }</td>
                <td style="text-align:right;font-weight:bold">${element.count}</span></td>
            </tr>`
                        
                    })

                    }
            
        </table>
                
                <h5 style="text-align:center">${subTotal} -------------------ٹوٹل</h5>
                      <p style="text-align:left;font-family: cursive;font-size:8px;font-weight: bold;">Developed by:Uqair Ali</p>
                <br/>
              
                  </body>
                  </html>`

            }
        ];
    }
    const onGoPrint = async () => {
        try {



            const websocket = await qz.websocket.connect({ retries: 0, delay: 1 })
            setIsConnect(qz.websocket.isActive)
            let config = await qz.configs.create("BC-95AC")
            console.log("testc", config)

            printData().push(); //cut paper


            await qz.print(config, printData()).then(successPrt=>{
                onBeforePrint().then(savedDt=>{
                    console.log(savedDt)
                    onSuccessPrint()
                })
            }).catch(err => {
                console.log(err)
            });

            await qz.websocket.disconnect()
            setIsConnect(qz.websocket.isActive)
           
        }
        catch (err) {
            console.log(err)
            setIspendingPrint(false)
        }
    }
    const onPrint = async () => {
        
            onGoPrint()
        

    }
    console.log("test123test", isConnect, receiptArray)

    return (
        <div>
            <Button
                disabled={!receiptArray.length || isPendingPrint}
                onClick={onPrint}
                className="btn-submit" fill type="submit"><i className={"fa fa-print"} />
                {"Print Recipt"}
            </Button>
        </div>
    )
}

export default NodeThermalPrinter