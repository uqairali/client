import React,{useState} from 'react'
import { Row,Col,ControlLabel,FormGroup } from 'react-bootstrap'
import Button from "components/CustomButton/CustomButton.jsx";

const CustomSelect=({auto,setAuto,reciptArray,setReciptArray})=>{
    const [item,setItem]=useState('')
    const [price,setPrice]=useState('')
    const [count,setCount]=useState(1)
    const handleSubmit=()=>{
     if(item===""||price<=0){
         return
     }
     var newArray = [...reciptArray]
     newArray.push({
         item: item,
         price: price,
         count: count,
         subTotal:price* count,
         itemId: "customEntry"
     })
     setReciptArray(newArray)
     setItem('')
     setPrice('')
     setCount(1)
    }
    return(
        <Row className="kata-item-waraper">
        <Col md={4}>
        <FormGroup>
                <ControlLabel className="urdu-labl">سامان</ControlLabel>
                <input
                    value={item}
                    onChange={e=>setItem(e.target.value)}
                    className="form-control"
                />
            </FormGroup>
        </Col>
        <Col md={3}>
            <FormGroup>
                <ControlLabel className="urdu-labl">قیمت</ControlLabel>
                <input
                onKeyPress={event => {
                    if (event.which == 13 || event.keyCode == 13) {
                        handleSubmit()
                    }
                }}
                type="number"
                    value={price}
                    onChange={e=>setPrice(e.target.value)}
                    className="form-control"
                />
            </FormGroup>
        </Col>
        <Col md={2}>
            <FormGroup>
                <ControlLabel className="urdu-labl">نعداد</ControlLabel>
                <input
                    onKeyPress={event => {
                        if (event.which == 13 || event.keyCode == 13) {
                            handleSubmit()
                        }
                    }}
                    onChange={e => setCount(e.target.value)}
                    type="number"
                    value={count}
                    className="form-control"
                />
            </FormGroup>
        </Col>
      
        <Col md={3}>
            <FormGroup>
                <ControlLabel className="urdu-labl">ٹوٹل</ControlLabel>
                <input
                    readOnly={true}
                    value={price * count}
                    className="form-control"
                />
            </FormGroup>
        </Col>
       
<Col md={7}></Col>
        <Col md={2}>
            <FormGroup>
                <ControlLabel className="urdu-labl">آٹو</ControlLabel>
                <div className="urdu-btn-text">
                    <Button
                        onClick={() => setAuto(!auto)}
                        className="btn-submit" fill>
                        <i className={auto ? "fa fa-eye" : "fa fa-eye-slash"} />{auto ? 'ON' : 'OFF'}
                    </Button>
                </div>
            </FormGroup>

        </Col>
        <Col md={3}>
            <FormGroup>
                <ControlLabel className="urdu-labl">سبمیٹ</ControlLabel>
                <div className="urdu-btn-text">
                    <Button
                        disabled={item===""||price<=0}
                        onClick={handleSubmit}
                        className="btn-submit" fill>
                        <i className={"fa fa-check"} />جاری  رکیں
        </Button>
                </div>
            </FormGroup>

        </Col>
    </Row>
    )
}

export default CustomSelect