import React, { useState,useEffect } from 'react'
import { Grid, Modal, FormGroup, ControlLabel } from 'react-bootstrap'
import { editAddress } from '../../api/api'
import Select from "react-select";
import Button from "components/CustomButton/CustomButton.jsx";
import { selectOptions } from 'variables/Variables';

const SwitchAddreses = ({ show, hide, onShowNotification, selectedObj, Data, onDeleteAddress }) => {
    const [selectedAddress, setSelectedAddress] = useState([])
    const [allAddreses,setAllAddreses]=useState([])
  useEffect(()=>{
    const filterData =Data.filter((fitm)=>fitm.address!==selectedObj.address)
    const mapData=filterData.map(itm => {
            return { label: itm.address, value: itm.address, data: itm }
    })
    setAllAddreses(mapData)
  },[])
  const onEditAddress=async()=>{
      const newData={
          names:selectedAddress.data.names
      }
      newData.names.concat(selectedObj.names)
      try{
        var doc=await editAddress(selectedAddress.data._id,newData)
        if(doc.data){
            hide()
            onDeleteAddress()
            onShowNotification("info","آپڈیٹ  ھو  چکا  ھیں")
        }
      }catch(err){
          onShowNotification("error",err.message)
      }
  }
   
    return (
        <Grid fluid>
            <Modal className="modal-custom-wrapper" show={show} onHide={hide} >
                <Modal.Header closeButton>
                    <Modal.Title>
                        ڈیلیٹ  کردہ  گاوں  کے  لوگ  آپ  کس  گاوں  میں  ڈالنا  چاہتے  ہیں
             </Modal.Title>
                </Modal.Header>
                <Modal.Body>

                    <FormGroup>
                        <ControlLabel>گاوں</ControlLabel>
                        <Select
                            clearable={false}
                            placeholder={"Single Select"}
                            value={selectedAddress}
                            options={allAddreses}
                            onChange={data => {
                                if (data) {
                                    setSelectedAddress(data)
                                }
                            }}
                        />
                        {!selectedAddress.label&&<small className="text-danger">گاوں  منتخب  کریں</small>}
                    </FormGroup>

                    <div className="center urdu-btn-text">
                        <Button onClick={onEditAddress} className="btn-submit" fill disabled={!selectedAddress.label}>
                            <i className={"fa fa-check"} />جاری  رکیں
                                </Button>
                    </div>
                </Modal.Body>
            </Modal>

        </Grid>
    )
}

export default SwitchAddreses