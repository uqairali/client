import React from 'react'
import { Grid, Modal, Form, FormGroup, ControlLabel } from 'react-bootstrap'
import useForm from "react-hook-form";
import { addNewItem } from '../../api/api'
import Button from "components/CustomButton/CustomButton.jsx";
const AddNewItem = ({ show, hide, fetchData, onShowNotification }) => {
    const { handleSubmit, register, errors, reset } = useForm();
    const onSubmit = async (value) => {

        const data = {
            name: value.name,
            price: value.price,
            shortKey: value.shortKey ? value.shortKey :value.name[0]+value.price,
            count: value.count ? value.count : 0
        }
   
        try {
            const doc = await addNewItem(data)
            if (doc.data) {
                hide()
                fetchData()
                onShowNotification("success", "نیا سٹاک  درج  ھوگیا‏ں  ہیں")
            }
        } catch (err) {
            onShowNotification("error", err.message)
        }
    }
    return (
        <Grid fluid>
            <Modal className="modal-custom-wrapper" show={show} onHide={hide} >
                <Modal.Header closeButton>
                    <Modal.Title>
                        نیاسٹاک  بناے
          </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form onSubmit={handleSubmit(onSubmit)} autoComplete="off">
                        <FormGroup
                            validationState={errors.name && errors.name.message ? "error" : "success"}
                        >
                            <ControlLabel>سٹاک  نام</ControlLabel>
                            <input
                                name="name"
                                ref={register({
                                    required: 'Required',
                                })}
                                placeholder="Enter Item Name"
                                className="form-control"
                            />
                            {(errors.name && errors.name.message) && <small className="text-danger">{errors.name && errors.name.message}</small>}
                        </FormGroup>


                        <FormGroup
                            validationState={errors.price && errors.price.message ? "error" : "success"}
                        >
                            <ControlLabel>قیمٹ</ControlLabel>
                            <input
                                type="number"
                                name="price"
                                ref={register({
                                    required: 'Required',
                                })}
                                placeholder="Enter Price"
                                className="form-control"

                            />
                            {(errors.price && errors.price.message) && <small className="text-danger">{errors.price && errors.price.message}</small>}
                        </FormGroup>
                        <FormGroup
                            validationState={errors.count && errors.count.message ? "error" : "success"}
                        >
                            <ControlLabel>نعداد</ControlLabel>
                            <input
                                type="number"
                                name="count"
                                ref={register({
                                    required: 'Required',
                                })}
                                placeholder="Enter Count"
                                className="form-control"

                            />
                            {(errors.count && errors.count.message) && <small className="text-danger">{errors.count && errors.count.message}</small>}
                        </FormGroup>
                        <FormGroup
                            validationState={errors.shortKey && errors.shortKey.message ? "error" : "success"}
                        >
                            <ControlLabel>شارٹ  کی</ControlLabel>
                            <input
                                type="string"
                                name="shortKey"
                                ref={register({
                                    required: false,
                                })}
                                placeholder="Enter Short Key"
                                className="form-control"

                            />
                            {(errors.shortKey && errors.shortKey.message) && <small className="text-danger">{errors.shortKey && errors.shortKey.message}</small>}
                        </FormGroup>

                        <div className="urdu-btn-text">
                            <Button
                                className="btn-submit" fill type="submit">
                                <i className={"fa fa-check"} />جاری  رکیں
                                </Button>
                        </div>

                    </Form>

                </Modal.Body>
            </Modal>

        </Grid>
    )
}


export default AddNewItem