import React, { useState } from 'react'
import { Grid, Modal, FormGroup, ControlLabel, Table } from 'react-bootstrap'
import { dateFormate2 } from 'utils/helper'
import Print from 'components/printer'
const ViewPrintModal = ({ selectedObj, show, hide }) => {
    const [isPendingPrint, setIspendingPrint] = useState(false)
    console.log("test123", selectedObj)
    const onBeforePrint = () => {
        return new Promise(async (resolve, reject) => {
            setIspendingPrint(true)
            resolve(true)
        })
    }
    return (
        <Grid fluid>
            <Modal className="modal-custom-wrapper" show={show} onHide={hide} >
                <Modal.Header closeButton>
                    <Modal.Title>
                        {selectedObj.clientName.name}
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Table responsive className="custom-table-wrapper">
                        <thead>

                            <tr>
                                <th>روپے</th>
                                <th>نرخ</th>
                                <th>تفسیل</th>
                                <th>تعداد</th>
                                <th style={{ textAlign: "center" }}>#</th>
                            </tr>
                        </thead>
                        <tbody>

                            {
                                selectedObj.recipt.map((element, key) => (
                                    <tr key={key}>
                                        <th>{element.subTotal}</th>
                                        <th>{element.price}</th>
                                        <th>{element.item}</th>
                                        <th>{element.count}</th>
                                        <th style={{ textAlign: "center" }}>{key + 1}</th>

                                    </tr>
                                ))
                            }

                        </tbody>
                    </Table>
                    {
                        selectedObj.recipt.length ?
                            <FormGroup>
                                <ControlLabel style={{
                                    textAlign: 'center',
                                    fontWeight: 'bold',
                                    fontSize: "14px"
                                }}>ٹوٹل</ControlLabel>
                                <input
                                    type={"number"}
                                    className="form-control"
                                    value={selectedObj.totalAmmount}
                                    readOnly={true}
                                />
                            </FormGroup> : null
                    }
                    <Print
                        onSuccessPrint={hide}
                        onBeforePrint={onBeforePrint}
                        receiptArray={selectedObj.recipt}
                        date={dateFormate2(new Date())}
                        clientName={selectedObj.clientName.name}
                        serialNumber={selectedObj.serialNumber}
                        isPendingPrint={isPendingPrint}
                        setIspendingPrint={setIspendingPrint}
                    />
                </Modal.Body>
            </Modal>

        </Grid>
    )
}

export default ViewPrintModal