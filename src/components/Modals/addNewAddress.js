import React, { useState } from 'react'
import { Grid, Modal, Form, FormGroup, ControlLabel } from 'react-bootstrap'
import useForm from "react-hook-form";
import { newAddress } from '../../api/api'
import Button from "components/CustomButton/CustomButton.jsx";

const AddNewAddress = ({ show, hide, fetchData, onShowNotification }) => {
    const [allSellsMen, setAllSellsMen] = useState([])


    const { handleSubmit, register, errors } = useForm();
    const onSubmit = async (values) => {
        try {
            const data = {
                address: values.address,
            }
            var res = await newAddress(data)
            if (res.data) {
                hide()
                onShowNotification('success', "نیا  گاوں  سیو  ھو  چکا  ھیں")
                fetchData()
            }

        } catch (err) {
            onShowNotification('error', err.message)
        }

    }
    return (
        <Grid fluid>
            <Modal className="modal-custom-wrapper" show={show} onHide={hide} >
                <Modal.Header closeButton>
                    <Modal.Title>
                        نیا  گاوں  کا  نام  درج  کریں
             </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form onSubmit={handleSubmit(onSubmit)} autoComplete="off">

                        <FormGroup
                            validationState={errors.address && errors.address.message ? "error" : "success"}
                        >
                            <ControlLabel>گاوں</ControlLabel>
                            <input
                                name="address"
                                ref={register({
                                    required: 'Required',
                                })}
                                placeholder="Enter Address"
                                className="form-control"
                                type="string"
                            />
                            {(errors.address && errors.address.message) &&
                                <small className="text-danger">{errors.address && errors.address.message}</small>}
                        </FormGroup>

                        <div className="center urdu-btn-text">
                            <Button className="btn-submit" fill type="submit">
                                <i className={"fa fa-check"} />جاری  رکیں</Button>
                        </div>
                    </Form>
                </Modal.Body>
            </Modal>

        </Grid>
    )
}

export default AddNewAddress