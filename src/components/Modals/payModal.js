import React, { useState } from 'react'
import { Grid, Modal, Form, FormGroup, ControlLabel } from 'react-bootstrap'
import useForm from "react-hook-form";
import { newPay } from '../../api/api'
import Datetime from "react-datetime";
import Button from "components/CustomButton/CustomButton.jsx";
const AddPayment = ({ show, hide, fetchData, onShowNotification, selectedObj }) => {
    const [date,setDate]=useState(new Date())
console.log(selectedObj)
    const { handleSubmit, register, errors, reset } = useForm();
    const onSubmit = async (value) => {

        const data = {
         ammount:value.ammount,
         name:selectedObj._id
        }
        try {
            const doc = await newPay(data,date)
            if (doc.data) {
                hide()
                fetchData()
                onShowNotification("success", "وصول  ھو  گیاں  ھیں")
            }
        } catch (err) {
            onShowNotification("error", err.message)
        }
    }
    return (
        <Grid fluid>
            <Modal className="modal-custom-wrapper" show={show} onHide={hide} >
                <Modal.Header closeButton>
                    <Modal.Title>
                        پیسے  وصول  کریں
          </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form onSubmit={handleSubmit(onSubmit)} autoComplete="off">
                        <FormGroup
                            validationState={errors.name && errors.name.message ? "error" : "success"}
                        >
                            <ControlLabel>نام</ControlLabel>
                            <input
                                readOnly={true}
                                defaultValue={selectedObj.name+" "+selectedObj.address.address}
                                className="form-control"
                            />
                        </FormGroup>
                        <FormGroup>
                            <ControlLabel >تاریخ</ControlLabel>
                            <Datetime
                                timeFormat={false}
                                inputProps={{ placeholder: "Select date" }}
                                defaultValue={new Date()}
                                onChange={(d) => setDate(d)}
                                value={date}
                                closeOnSelect={true}
                            />
                        </FormGroup>

                        <FormGroup
                            validationState={errors.ammount && errors.ammount.message ? "error" : "success"}
                        >
                            <ControlLabel>رقم</ControlLabel>
                            <input
                                type="number"
                                name="ammount"
                                ref={register({
                                    required: "Required",
                                })}
                                placeholder="Enter ammount"
                                className="form-control"

                            />
                            {(errors.ammount && errors.ammount.message) && <small className="text-danger">{errors.ammount && errors.ammount.message}</small>}
                        </FormGroup>

                        <div className="urdu-btn-text">
                            <Button
                                className="btn-submit" fill type="submit">
                                <i className={"fa fa-check"} />جاری  رکیں
                                </Button>
                        </div>

                    </Form>

                </Modal.Body>
            </Modal>

        </Grid>
    )
}


export default AddPayment