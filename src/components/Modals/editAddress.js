import React, { useState } from 'react'
import { Grid, Modal, Form, FormGroup, ControlLabel } from 'react-bootstrap'
import useForm from "react-hook-form";
import { editAddress } from '../../api/api'
import Button from "components/CustomButton/CustomButton.jsx";

const EditAddressModal = ({ show, hide, fetchData, onShowNotification,selectedObj }) => {
    const { handleSubmit, register, errors } = useForm();
    const onSubmit = async (values) => {
        try {
            const data = {
                address: values.address,
            }
            var res = await editAddress(selectedObj._id,data)
            if (res.data) {
                hide()
                fetchData()
                onShowNotification('success', "نیا  گاوں آپڈیٹ  ھو  چکا  ھیں")
            }

        } catch (err) {
            onShowNotification('error', err.message)
        }

    }
    return (
        <Grid fluid>
            <Modal className="modal-custom-wrapper" show={show} onHide={hide} >
                <Modal.Header closeButton>
                    <Modal.Title>
                        نیا  گاوں  کا  نام  درج  کریں
             </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form onSubmit={handleSubmit(onSubmit)} autoComplete="off">

                        <FormGroup
                            validationState={errors.address && errors.address.message ? "error" : "success"}
                        >
                            <ControlLabel>گاوں</ControlLabel>
                            <input
                            defaultValue={selectedObj.address}
                                name="address"
                                ref={register({
                                    required: 'Required',
                                })}
                                placeholder="Enter Address"
                                className="form-control"
                                type="string"
                            />
                            {(errors.address && errors.address.message) &&
                                <small className="text-danger">{errors.address && errors.address.message}</small>}
                        </FormGroup>

                        <div className="center urdu-btn-text">
                            <Button className="btn-submit" fill type="submit">
                                <i className={"fa fa-check"} />جاری  رکیں</Button>
                        </div>
                    </Form>
                </Modal.Body>
            </Modal>

        </Grid>
    )
}

export default EditAddressModal