import React, { useState, useEffect } from 'react'
import { Grid, Modal, Form, FormGroup, ControlLabel } from 'react-bootstrap'
import useForm from "react-hook-form";
import { alladdreses,getAccountId,newAccount,incrementAccountId } from '../../api/api'
import Button from "components/CustomButton/CustomButton.jsx";
import Select from "react-select";
const AddNewAccount = ({ show, hide, fetchData, onShowNotification }) => {
    const [selectedAddress, setSelectedAddress] = useState({})
    const [AllAddreses, setAllAddreses] = useState([])
    const [accountId,setAccountId]=useState([])
    useEffect(() => {
        onGetAllAddreses()
        onGetAccountId()
    }, [])
    const onGetAccountId=async()=>{
        try{
        var doc=await getAccountId()
        if(doc.data){
            setAccountId(doc.data)
        }
        }catch(err){
            onShowNotification("error",err.message)
        }
    }
    const onGetAllAddreses = async () => {
        try {
            const data = await alladdreses()
            if (data.data) {
                const mapData = data.data.map(itm => {
                    return { label: itm.address, value: itm.address, data: itm }
                })
                setAllAddreses(mapData)
            }
        } catch (err) {
            onShowNotification('error', err.message)
        }
    }
    const { handleSubmit, register, errors, reset } = useForm();
    const onSubmit = async (value) => {
        if (!selectedAddress.label) {
            onShowNotification("info", "درجہ  ذیل  ڈیٹا  درج  کریں")
            return
        }
        const data = {
           name:value.name,
           address:selectedAddress.data._id,
           accountLimit:value.accountLimit?value.accountLimit:0,
           ammount:0,
           accountId:accountId[0].accountId,
           contactNumber:value.contactNumber
        }
        try {
    const doc=await newAccount(data)
    await incrementAccountId(accountId[0]._id)
    if(doc.data){
        hide()
        fetchData()
        onShowNotification("success","نیا  اکاونٹ  درج  ھوگیا‏ں  ہیں")
    }
        } catch (err) {
            onShowNotification("error", err.message)
        }
    }
    return (
        <Grid fluid>
            <Modal className="modal-custom-wrapper" show={show} onHide={hide} >
                <Modal.Header closeButton>
                    <Modal.Title>
                        نیااکاونٹ  بناے
          </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form onSubmit={handleSubmit(onSubmit)} autoComplete="off">
                        <FormGroup
                            validationState={errors.name && errors.name.message ? "error" : "success"}
                        >
                            <ControlLabel>نام</ControlLabel>
                            <input
                                name="name"
                                ref={register({
                                    required: 'Required',
                                })}
                                placeholder="Enter Customer Name"
                                className="form-control"
                            />
                            {(errors.name && errors.name.message) && <small className="text-danger">{errors.name && errors.name.message}</small>}
                        </FormGroup>

                        <FormGroup>
                            <ControlLabel>گاوں</ControlLabel>
                            <Select
                                clearable={false}
                                placeholder={"Single Select"}
                                value={selectedAddress}
                                options={AllAddreses}
                                onChange={data => {
                                    if (data) {
                                        setSelectedAddress(data)
                                    }
                                }}
                            />
                            {!selectedAddress.label && <small className="text-danger">گاوں  منتخب  کریں</small>}
                        </FormGroup>
                        <FormGroup
                            validationState={errors.contactNumber && errors.contactNumber.message ? "error" : "success"}
                        >
                            <ControlLabel>موبایل  نمبر</ControlLabel>
                            <input
                                name="contactNumber"
                                ref={register({
                                    required: false,
                                })}
                                placeholder="Enter Mobile Number"
                                className="form-control"
                            />
                            {(errors.contactNumber && errors.contactNumber.message) && <small className="text-danger">{errors.contactNumber && errors.contactNumber.message}</small>}
                        </FormGroup>
                        <FormGroup
                            validationState={errors.accountLimit && errors.accountLimit.message ? "error" : "success"}
                        >
                            <ControlLabel>حد ادھار</ControlLabel>
                            <input
                                type="number"
                                name="accountLimit"
                                ref={register({
                                    required: false,
                                })}
                                placeholder="Enter Account Limit"
                                className="form-control"

                            />
                            {(errors.accountLimit && errors.accountLimit.message) && <small className="text-danger">{errors.accountLimit && errors.accountLimit.message}</small>}
                        </FormGroup>

                        <div className="urdu-btn-text">
                            <Button
                                className="btn-submit" fill type="submit">
                                <i className={"fa fa-check"} />جاری  رکیں
                                </Button>
                        </div>

                    </Form>

                </Modal.Body>
            </Modal>

        </Grid>
    )
}


export default AddNewAccount