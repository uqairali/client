import React, { useState, useEffect, Fragment } from 'react'
import { connect } from 'react-redux'
import { Row, Col, FormGroup, ControlLabel, Table } from 'react-bootstrap'
import Card from "components/Card/Card.jsx";
import { getAccountById, getReciptByID, onUpdateRecipt, getAllItems, getReciptSN, newRecipt, UpdateReciptSN } from 'api/api'
import Select from "react-select";
import Datetime from "react-datetime";
import Button from "components/CustomButton/CustomButton.jsx";
import { onCalculateSubTotal, paramsDateFormet } from 'utils/helper'
import DeleteAlert from 'components/Modals/deleteAlert'
import { useSpeechSynthesis } from 'react-speech-kit';
import CustomSelect from 'components/custumEntry'
import { icons } from 'utils/const'
const Kata = (props) => {
    const [currentUserId, setCurrentUserId] = useState('')
    const [currentUserDetails, setCurrentUserDetails] = useState({})
    const [allItems, setAllItems] = useState([])
    const [selectedItem, setSelectedItem] = useState({})
    const [count, setCount] = useState(1)
    const [shortKey, setShortKey] = useState('')
    const [reciptArray, setReciptArray] = useState([])
    const [onShowLimitAlert, setOnShowLimitAlert] = useState(false)
    const [auto, setAuto] = useState(true)
    const [consattion, setConsattion] = useState('')
    const [loading, setLoading] = useState(false)
    const { speak } = useSpeechSynthesis();
    const [updateRecipt, setUpdateRecipt] = useState({})
    const [date,setDate]=useState(new Date())

    useEffect(() => {
        setCurrentUserId(props.match.params.id)
        onGetCurrentUser()
        onGetAllItems()
    }, [])

    const onGetCurrentUser = async () => {
        try {
            const user = await getAccountById(props.match.params.id)

            if (user.data) {
                setCurrentUserDetails(user.data)
            }
            if (props.match.params.reciptId) {
                var recipt = await getReciptByID(props.match.params.reciptId)
                setUpdateRecipt(recipt.data[0])
                setReciptArray(recipt.data[0].recipt)
                setDate(new Date(recipt.data[0].date))
            }
        } catch (err) {
            props.onShowNotification("error", err.message)
        }
    }
    const onGetAllItems = async () => {
        try {
            const items = await getAllItems()
            if (items.data) {
                const mapData = items.data.map(itm => {
                    return { label: itm.name, value: itm.name, data: itm }
                })
                setAllItems(mapData)
            }
        } catch (err) {
            props.onShowNotification("error", err.message)
        }
    }
    const ConfirmLimit = () => {
        var newArray = [...reciptArray]
        newArray.push({
            item: selectedItem.label,
            price: selectedItem.data.price - consattion,
            count: count,
            subTotal: (selectedItem.data.price - consattion) * count,
            itemId: selectedItem.data._id
        })
        setReciptArray(newArray)
        setSelectedItem({})
        setCount(1)
        setConsattion('')
        setShortKey('')
    }
    const handleSubmit = () => {
        if (selectedItem.data) {
            if (currentUserDetails.accountLimit > 0 && ((onCalculateSubTotal(reciptArray) + selectedItem.data.price * count) + currentUserDetails.ammount >= currentUserDetails.accountLimit)) {
                speak({ text: "limit puri ho chuki hin" })
                setOnShowLimitAlert(true)
            } else {
                var newArray = [...reciptArray]
                newArray.push({
                    item: selectedItem.label,
                    price: selectedItem.data.price - consattion,
                    count: count,
                    subTotal: (selectedItem.data.price - consattion) * count,
                    itemId: selectedItem.data._id
                })
                setReciptArray(newArray)
                setSelectedItem({})
                setCount(1)
                setConsattion('')
                setShortKey('')
            }
        } else {
            return
        }
    }
    const onFilterShortKey = (e) => {
        setShortKey(e.target.value)
        const onFilter = allItems.filter(itm => itm.data.shortKey === e.target.value)
        if (onFilter.length >= 1)
            setSelectedItem(onFilter[0])
        else {
            setSelectedItem({})
        }

    }
    const handleSaveData = async () => {
        if (props.match.params.reciptId) {
            handleUpdateData()
        } else {
            try {
                setLoading(true)
                const rsn = await getReciptSN()
                const data = {
                    clientName: currentUserId,
                    totalAmmount: onCalculateSubTotal(reciptArray),
                    serialNumber: rsn.data.serialNumber,
                    recipt: reciptArray
                }
                const saveRecipt = await newRecipt(paramsDateFormet(date),data)
                await UpdateReciptSN(rsn.data._id)
                onGetCurrentUser()

                setReciptArray([])
                setLoading(false)
                props.onShowNotification("success", "بل  جمہ  ھوگیا  ھیں")
                onChangeRoute()
            } catch (err) {
                setLoading(false)
                props.onShowNotification("error", err.message)
            }
        }
    }
   
    const onChangeRoute=()=>{
        setTimeout(() => {
            const {pathname}=props.history.location
            var newpath=pathname[7]+pathname[8]+pathname[9]+pathname[10]
            if(newpath==='kata')
            props.history.push('/admin/eBook')
        }, 6000);
    }
    const handleUpdateData = async () => {
        try {
            setLoading(true)
            const data = {
                clientName: currentUserId,
                totalAmmount: onCalculateSubTotal(reciptArray),
                serialNumber: updateRecipt.serialNumber,
                recipt: reciptArray
            }

            await onUpdateRecipt(props.match.params.reciptId, data,paramsDateFormet(date),updateRecipt.totalAmmount)
            setReciptArray([])
            setLoading(false)
            onGetCurrentUser()
            props.onShowNotification("success", "بل  آپ  ڈیٹ  ھوگیا  ھیں")
            onChangeRoute()
        } catch (err) {
            setLoading(false)
            props.onShowNotification("error", err.message)
        }
    }
    const isItemSelected = selectedItem.data ? true : false;
    return (
        <div>
            <Row className="kata-wrapper">
                <Col md={7}>
                    <Card
                        title={
                            <Row>
                                <Col md={2}>تاریخ</Col>
                                <Col md={5}>
                                <Datetime
                                closeOnSelect={true}
                                timeFormat={false}
                                inputProps={{ placeholder: "Select date" }}
                                defaultValue={new Date()}
                                onChange={(d) => setDate(d)}
                                value={date}
                            />
                            </Col>
                            <Col md={5}>
                            {currentUserDetails.name &&
                            (currentUserDetails.name + " " +
                                currentUserDetails.address.address)}
                                </Col>
                            </Row>
                            }
                        ctAllIcons
                        content={
                            !auto ? <CustomSelect
                                auto={auto}
                                setAuto={setAuto}
                                reciptArray={reciptArray}
                                setReciptArray={setReciptArray}
                            />
                                :
                                <Row className="kata-item-waraper">
                                    <Col md={4}>
                                        <FormGroup>
                                            <ControlLabel className="urdu-labl">سامان</ControlLabel>
                                            <Select
                                                clearable={false}
                                                placeholder={"Single Select"}
                                                value={selectedItem}
                                                options={allItems}
                                                onChange={data => {
                                                    if (data) {
                                                        setSelectedItem(data)
                                                    }
                                                }}
                                            />
                                        </FormGroup>
                                    </Col>
                                    <Col md={3}>
                                        <FormGroup>
                                            <ControlLabel className="urdu-labl">قیمت</ControlLabel>
                                            <input
                                                readOnly={true}
                                                value={selectedItem.data ? selectedItem.data.price : ''}
                                                className="form-control"
                                            />
                                        </FormGroup>
                                    </Col>
                                    <Col md={2}>
                                        <FormGroup>
                                            <ControlLabel className="urdu-labl">نعداد</ControlLabel>
                                            <input
                                                onKeyPress={event => {
                                                    if (event.which == 13 || event.keyCode == 13) {
                                                        handleSubmit()
                                                    }
                                                }}
                                                disabled={!isItemSelected}
                                                onChange={e => setCount(e.target.value)}
                                                type="number"
                                                value={count}
                                                className="form-control"
                                            />
                                        </FormGroup>
                                    </Col>
                                    <Col md={3}>
                                        <FormGroup>
                                            <ControlLabel className="urdu-labl">رعایت</ControlLabel>
                                            <input
                                                onKeyPress={event => {
                                                    if (event.which == 13 || event.keyCode == 13) {
                                                        handleSubmit()
                                                    }
                                                }}
                                                type="number"
                                                readOnly={!isItemSelected}
                                                value={consattion}
                                                onChange={e => setConsattion(e.target.value)}
                                                className="form-control"
                                            />
                                        </FormGroup>
                                    </Col>
                                    <Col md={4}>
                                        <FormGroup>
                                            <ControlLabel className="urdu-labl">ٹوٹل</ControlLabel>
                                            <input
                                                readOnly={true}
                                                value={selectedItem.data ? (selectedItem.data.price - consattion) * count : ''}
                                                className="form-control"
                                            />
                                        </FormGroup>
                                    </Col>
                                    <Col md={3}>
                                        <FormGroup>
                                            <ControlLabel className="urdu-labl">شارٹ  کٹ</ControlLabel>
                                            <input
                                                onKeyPress={event => {
                                                    if (event.which == 13 || event.keyCode == 13) {
                                                        handleSubmit()
                                                    }
                                                }}
                                                onChange={onFilterShortKey}
                                                value={shortKey}
                                                className="form-control"
                                            />
                                        </FormGroup>
                                    </Col>

                                    <Col md={2}>
                                        <FormGroup>
                                            <ControlLabel className="urdu-labl">آٹو</ControlLabel>
                                            <div className="urdu-btn-text">
                                                <Button
                                                    onClick={() => setAuto(!auto)}
                                                    className="btn-submit" fill>
                                                    <i className={auto ? "fa fa-eye" : "fa fa-eye-slash"} />{auto ? 'ON' : 'OFF'}
                                                </Button>
                                            </div>
                                        </FormGroup>

                                    </Col>
                                    <Col md={3}>

                                        <FormGroup>
                                            <ControlLabel className="urdu-labl">سبمیٹ</ControlLabel>
                                            <div className="urdu-btn-text">
                                                <Button
                                                    disabled={!isItemSelected || loading}
                                                    onClick={handleSubmit}
                                                    className="btn-submit" fill>
                                                    <i className={''} />جاری  رکیں
                                </Button>
                                            </div>
                                        </FormGroup>

                                    </Col>
                                </Row>
                        }
                    />
                </Col>

                <Col md={5}>
                    <Card
                        title={"بل"}
                        ctAllIcons
                        content={
                            <div>
                                <div className="recipt-show">
                                    <Table responsive>
                                        <thead>

                                            <tr>
                                                <th>روپے</th>
                                                <th>نرخ</th>
                                                <th>تفسیل</th>
                                                <th>تعداد</th>
                                                <th style={{ textAlign: "center" }}>#</th>
                                                <th>X</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <Fragment>
                                                {
                                                    reciptArray.map((element, key) => {
                                                        return <tr key={key}>
                                                            <th>{element.subTotal}</th>
                                                            <th>{element.price}</th>
                                                            <th>{element.item}</th>
                                                            <th>{element.count}</th>
                                                            <th style={{ textAlign: "center" }}>{key + 1}</th>
                                                            <th onClick={() => {
                                                                var newArray = [...reciptArray]
                                                                newArray.splice(key, 1)
                                                                setReciptArray(newArray)
                                                            }}>
                                                                < i style={{
                                                                   fontWeight:'bold', color: "red", cursor: "pointer",fontSize:'16px'
                                                                }} className={icons.delete} />
                                                            </th>
                                                        </tr>

                                                    })
                                                }

                                            </Fragment>
                                        </tbody>
                                    </Table>


                                </div>
                                <Row>
                                    <Col md={8}>
                                        <FormGroup>
                                            <input
                                                style={{ fontWeight: 'bold', color: '#ec370e', fontSize: '25px' }}
                                                type={"number"}
                                                className="form-control"
                                                value={onCalculateSubTotal(reciptArray)}
                                                readOnly={true}
                                            />
                                        </FormGroup>
                                    </Col>
                                    <Col md={4}>
                                        <Col style={{ fontSize: "20px", fontWeight: 'bold' }} md={4}>ٹوٹل</Col>
                                    </Col>
                                </Row>
                                <Row>
                                    <div className="urdu-btn-text">
                                        <Button
                                            disabled={reciptArray.length < 1}
                                            onClick={handleSaveData}
                                            className="btn-submit" fill>
                                            <i className={''} />محفوض  کریں
                                </Button>
                                    </div>
                                </Row>
                            </div>}
                    />
                </Col>
            </Row>
            <Row className="kata-wrapper">
            <Card
                title={'ریکارڈ'}
                ctAllIcons
                content={
                    <div className="recipt-show">
                        {
                            currentUserDetails.name &&
                            <Table responsive>
                                <thead>

                                    <tr className="user-details-header">
                                        <th>نام</th>
                                        <th>ای  ڈی</th>
                                        <th>گاو‏ں</th>
                                        <th>حدادھار</th>
                                        <th>ٹوٹل  رقم</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <Fragment>
                                        {<tr>
                                            <th>{currentUserDetails.name}</th>
                                        <th>{currentUserDetails.accountId}</th>
                                            <th>{currentUserDetails.address.address}</th>
                                            <th>{currentUserDetails.accountLimit}</th>
                                            <th>{currentUserDetails.ammount}</th>

                                        </tr>


                                        }

                                    </Fragment>
                                </tbody>
                            </Table>

                        }
                    </div>
                }
            />
            </Row>
            {
                onShowLimitAlert &&
                <DeleteAlert
                    buttonText={"جاری  رکیں"}
                    hide={() => setOnShowLimitAlert(false)}
                    onDelete={() => {
                        ConfirmLimit()
                    }}
                    text={"لیمیٹ  پوری  ھو  چکی  ہیں"}
                />
            }
        </div>
    )
}

const mapDispatchToProps = dispatch => {
    return {
        onShowNotification: (errorType, error) =>
            dispatch({
                type: "SET_NOTIFICATION",
                errorType: errorType,
                errorMessage: error
            }),
    }
}
export default connect(null, mapDispatchToProps)(Kata)