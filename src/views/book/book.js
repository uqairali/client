
import React, { useEffect, useState } from "react";
import {
    Grid,
    Col,
    FormGroup,
    ControlLabel,
    Row,
} from "react-bootstrap";
import Select from "react-select";
import Card from "components/Card/Card.jsx";
import Button from "components/CustomButton/CustomButton.jsx";
import { connect } from 'react-redux'
import { allAccounts, getAccountByKey } from 'api/api'
function Book(props) {
    const [allAccountsData, setAllAccount] = useState([])
    const [selectedAccount, setSelectedAccount] = useState({})
    const [accountId, setAccountId] = useState('')
    useEffect(() => {
        onGetAllAccount()
    }, [])
    const onGetAllAccount = async () => {
        try {
            const doc = await allAccounts()
            if (doc.data) {
                const mapData = doc.data.map(itm => {
                    return {
                        label: itm.name + "(" + itm.address.address + ")",
                        value: itm.name + "(" + itm.address.address + ")",
                        data: itm
                    }
                })
                setAllAccount(mapData)
            }
        } catch (err) {
            props.onShowNotification("error", err.message)
        }
    }
    const onGetAccountByKey = async (e) => {
        try {
            setAccountId(e.target.value)
            const filterData = allAccountsData.filter(itm => itm.data.accountId === e.target.value)
            if (filterData.length) {
                setSelectedAccount({
                    label: filterData[0].label,
                    value: filterData[0].label,
                    data: filterData[0].data
                })
            } else {
                setSelectedAccount({})
            }
        } catch (err) {
            props.onShowNotification("error", err.message)
        }
    }
    const onSubmit = () => {
        if (selectedAccount.data)
            props.history.push(`/admin/kata/${selectedAccount.data._id}`)
        else
            props.onShowNotification("info", "پہلے  اکاونٹ  منتخب  کریں")
    }
    return (
        <div className="login-wrapper ebook">
            <section className="about">
            </section>
            <Grid fluid>
                
                <Row>
                    <Col md={6} sm={4} mdOffset={3} smOffset={3}>
                        <Card
                            title="روز  نامچہ"
                            ctAllIcons
                            content={
                                <form onSubmit={onSubmit}>
                                    <FormGroup >
                                        <ControlLabel style={{
                                            fontWeight: "bold",
                                            fontSize: '18px'
                                        }}>اکاونٹ  ای  ڈی  ڈالیں</ControlLabel>
                                        <input
                                            onKeyPress={event => {
                                                if (event.which == 13 || event.keyCode == 13) {
                                                    onSubmit()
                                                }
                                            }}
                                            value={accountId}
                                            onChange={onGetAccountByKey}
                                            type="number"
                                            placeholder="Enter Account Id"
                                            className="form-control"
                                        />
                                    </FormGroup>
                                    <FormGroup >
                                        <ControlLabel style={{
                                            fontWeight: "bold",
                                            fontSize: '18px'
                                        }}>یا  منتخب  کریں</ControlLabel>
                                        <Select
                                            clearable={false}
                                            placeholder={"Single Select"}
                                            value={selectedAccount}
                                            options={allAccountsData}
                                            onChange={data => {
                                                if (data) {
                                                    setSelectedAccount(data)
                                                    setAccountId(data.data.accountId)
                                                }
                                            }}
                                        />
                                    </FormGroup>
                                    <div className="urdu-btn-text">
                                        <Button
                                            type="submit"
                                            className="btn-submit" fill>
                                            <i className={"fa fa-check"} />جاری  رکیں
                                </Button>
                                    </div>
                                </form>
                            } />

                    </Col>

                </Row>
            </Grid>
        </div >
    );

}
const mapDispatchToProps = dispatch => {
    return {
        onShowNotification: (errorType, error) =>
            dispatch({
                type: "SET_NOTIFICATION",
                errorType: errorType,
                errorMessage: error
            }),
    }
}
export default connect(null, mapDispatchToProps)(Book);
