import React from 'react'
import ReactTable from "react-table";
import Button from "components/CustomButton/CustomButton.jsx";
import { deletePys, allPays } from 'api/api'
import DeleteAlert from '../../components/Modals/deleteAlert'
import { filterCaseInsensitive, dateFormate2 } from 'utils/helper';
import { connect } from 'react-redux'
import UpdatePay from 'components/Modals/editPayModal'
import { icons } from 'utils/const'
class Payment extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: false,
            selectedObj: "",
            showDeleteModal: false,
            Data: [],
            showEditModal: false
        }
        this.getAllPayes()
    }


    getAllPayes = async () => {
        try {
            const res = await allPays()

            this.setState({ selectedAccount: {}, Data: res.data, accountId: '' })
        } catch (err) {

        }
    }


    onConfirmDelete = async () => {
        try {
            await deletePys(this.state.selectedObj._id)
            this.getAllPayes()
            this.props.onShowNotification("success", "رسید  ختم  کی  گی  ہیں")
        } catch (err) {
            this.props.onShowNotification("error", err.message)
        }
    }
    render() {
        const { Data, selectedObj, loading, showDeleteModal, showEditModal } = this.state
        const data = Data.map((prop, sn) => {
            return {
                date: dateFormate2(prop.date),
                name: prop.name ? prop.name.name : "deleted",
                payment: prop.ammount,
                actions: <div className="table-action-btn-wrapper">

                    <Button onClick={() => {
                        this.setState({ selectedObj: prop }, () => {
                            this.setState({ showEditModal: true })
                        })
                    }} title="Edit" bsStyle="success"
                        bsSize="xs" fill >
                        < i className={icons.edit} />
                    </Button>
                    <Button onClick={() => {
                        this.setState({ selectedObj: prop }, () => this.setState({ showDeleteModal: true }))
                    }} title="Delete" bsStyle="danger"
                        bsSize="xs" fill >
                        < i className={icons.delete} />

                    </Button>
                </div>
            };
        })


        const Columns = [

            {
                Header: 'تاریخ',
                accessor: 'date',
                filterable: true,
                id: 'date'
            },
            {
                Header: "نام",
                accessor: "name",
                filterable: true,
                id: "name"
            },
            {
                Header: "وصول  رقم",
                accessor: "payment",
                filterable: true,
                id: "payment"
            },


            {

                Header: "actions",
                accessor: "actions",
                filterable: false,
            },


        ]
        return (

            <div className="react-table-custom-design">

                <ReactTable
                    title={"test"}
                    data={data}
                    filterable
                    columns={Columns}
                    defaultFilterMethod={(filter, row) => filterCaseInsensitive(filter, row)}
                    // manual
                    defaultPageSize={10}
                    // onFetchData={onFetch}
                    showPaginationBottom
                    showPaginationTop={false}
                    // pages={this.state.pages}
                    loading={loading}
                    sortable={false}
                    className="-striped -highlight"
                />

                {
                    showDeleteModal &&
                    <DeleteAlert
                        hide={() => this.setState({ showDeleteModal: false })}
                        onDelete={() => {
                            this.onConfirmDelete()
                        }}
                        text={(this.state.selectedObj.name ? this.state.selectedObj.name.name : "deleted") + " کیاں  آپ  یہ  ڈیلیٹ  کرنا  چھتے  ہے؟"}
                    />
                }


                {
                    showEditModal &&
                    <UpdatePay
                        show={showEditModal}
                        hide={() => this.setState({ showEditModal: false })}
                        selectedObj={selectedObj}
                        onShowNotification={this.props.onShowNotification}
                        fetchData={this.getAllPayes}
                    />
                }

               
            </div>
        )
    }
}


const mapDispatchToProps = dispatch => {
    return {
        onShowNotification: (errorType, error) =>
            dispatch({
                type: "SET_NOTIFICATION",
                errorType: errorType,
                errorMessage: error
            }),
    }
}
export default connect(null, mapDispatchToProps)(Payment)
