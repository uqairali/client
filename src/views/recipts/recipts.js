import React, { Fragment } from 'react'
import ReactTable from "react-table";
import Button from "components/CustomButton/CustomButton.jsx";
import { getAllRecipt, allAccounts, getReciptByName, deleteRecipt } from 'api/api'
import DeleteAlert from '../../components/Modals/deleteAlert'
import { Col, FormGroup, Row, ControlLabel } from 'react-bootstrap'
import { dateFormate2, paramsDateFormet, filterCaseInsensitive, calCulateTotalReciptAmmount } from 'utils/helper';
import Datetime from "react-datetime";
import Select from "react-select";
import { connect } from 'react-redux'
import { icons } from 'utils/const'
import ViewPrintModal from 'components/Modals/viewPrintModal'
class AllRecipt extends React.Component {
    constructor() {
        super()
        this.state = {
            Data: [],
            loading: false,
            selectedObj: "",
            deleteAlert: false,
            showViewModal: false,
            date: new Date(),
            showDeleteModal: false,
            AllClients: [],
            allAccounts: [],
            accountId: '',
            selectedAccount: {},
        }
        this.getProducts()
        this.onGetAllAccount()
    }

    onGetAllAccount = async () => {
        try {
            const doc = await allAccounts()
            if (doc.data) {
                const mapData = doc.data.map(itm => {
                    return {
                        label: itm.name + "(" + itm.address.address + ")",
                        value: itm.name + "(" + itm.address.address + ")",
                        data: itm
                    }
                })
                this.setState({ allAccounts: mapData })
            }
        } catch (err) {
            this.props.onShowNotification("error", err.message)
        }
    }
    getProducts = async () => {
        try {
            const res = await getAllRecipt(paramsDateFormet(this.state.date))

            this.setState({ selectedClient: [], Data: res.data, selectedAccount: {}, accountId: '' })
        } catch (err) {

        }
    }
    onGetAccountByKey = async (e) => {
        try {
            this.setState({ accountId: e.target.value })
            const filterData = this.state.allAccounts.filter(itm => itm.data.accountId === e.target.value)
            if (filterData.length) {
                this.setState({
                    selectedAccount: {
                        label: filterData[0].label,
                        value: filterData[0].label,
                        data: filterData[0].data
                    }
                })

            } else {
                this.setState({ selectedAccount: {} })
            }
        } catch (err) {
            this.props.onShowNotification("error", err.message)
        }
    }
    onGetReciptsByClientId = async () => {
        try {
            if (this.state.selectedAccount.data) {
                const docs = await getReciptByName(this.state.selectedAccount.data._id)
                this.setState({ Data: docs.data })
            }
        } catch (err) {
            this.props.onShowNotification("error", err.message)
        }
    }
    onConfirmDeleteRecipt = async () => {
        try {
            await deleteRecipt(this.state.selectedObj._id,
                this.state.selectedObj.clientName._id, this.state.selectedObj.totalAmmount)
            if (this.state.selectedAccount.data) {
                this.onGetReciptsByClientId()
            } else {
                this.getProducts()
            }
            this.props.onShowNotification("success", "رسید  ختم  کی  گی  ہیں")
        } catch (err) {
            this.props.onShowNotification("error", err.message)
        }
    }
    render() {
        const { date, showViewModal, Data, selectedObj, loading, deleteAlert } = this.state
        const data = Data.map((prop, sn) => {
            return {
                serialNumber: +prop.serialNumber,
                name: prop.clientName ? prop.clientName.name : <span style={{ color: 'red' }} >ڈیلیٹ  کردا</span>,
                address: prop.clientName ? prop.clientName.address.address : <span style={{ color: 'red' }} >ڈیلیٹ  کردا</span>,
                date: dateFormate2(prop.date),
                total: prop.totalAmmount,
                actions: <div className="table-action-btn-wrapper">
                    <Button onClick={() => {
                        this.setState({ selectedObj: prop }, () => this.setState({ showViewModal: true }))
                    }} title="View and Print" bsStyle="primary"
                        bsSize="xs" fill >

                        < i className={icons.print} />
                    </Button>
                    <Button onClick={() => {
                        window.open(`/admin/kata/${prop.clientName._id}/${prop._id}`)
                    }} title="Edit" bsStyle="success"
                        bsSize="xs" fill >
                        < i className={icons.edit} />
                    </Button>
                    <Button onClick={() => {
                        this.setState({ selectedObj: prop }, () => this.setState({ showDeleteModal: true }))
                    }} title="Delete" bsStyle="danger"
                        bsSize="xs" fill >
                        < i className={icons.delete} />

                    </Button>
                </div>
            };
        })


        const Columns = [

            {
                Header: 'ای  ڈی',
                accessor: 'serialNumber',
                filterable: true,
                id: 'serialNumber'
            },
            {
                Header: "نام",
                accessor: "name",
                filterable: true,
                id: "name"
            },
            {
                Header: "گاوں",
                accessor: "address",
                filterable: true,
                id: "address"
            },
            {
                Header: "ٹایم",
                accessor: "date",
                filterable: false,
                id: "date"
            },
            {
                Header: "ٹوٹل  بل",
                accessor: "total",
                filterable: false,
                id: "total"
            },
            {

                Header: "actions",
                accessor: "actions",
                filterable: false,
            },


        ]
        return (

            <div className="react-table-custom-design">
                <Row>
                    <Col md={3}>
                        <FormGroup>
                            <ControlLabel style={{
                                fontWeight: "bold",
                                fontSize: '18px'
                            }}>تاریخ</ControlLabel>
                            <Datetime
                                timeFormat={false}
                                inputProps={{ placeholder: "Select date" }}
                                defaultValue={new Date()}
                                onChange={(d) => this.setState({ date: d })}
                                value={date}
                                onBlur={() => this.getProducts({})}
                            />
                        </FormGroup>
                    </Col>
                    <Col md={3}>
                        <FormGroup >
                            <ControlLabel style={{
                                fontWeight: "bold",
                                fontSize: '18px'
                            }}>نام  منتخب  کریں </ControlLabel>
                            <Select
                                clearable={false}
                                placeholder={"Single Select"}
                                value={this.state.selectedAccount}
                                options={this.state.allAccounts}
                                onChange={data => {
                                    if (data) {
                                        this.setState({
                                            selectedAccount: data,
                                            accountId: data.data.accountId
                                        }, () => {
                                            this.onGetReciptsByClientId()
                                        })
                                    }
                                }}
                            />
                        </FormGroup>
                    </Col>
                    <Col md={3}>
                        <FormGroup >
                            <ControlLabel style={{
                                fontWeight: "bold",
                                fontSize: '18px'
                            }}>اکاونٹ  ای  ڈی  ڈالیں</ControlLabel>
                            <input
                                onKeyPress={event => {
                                    if (event.which == 13 || event.keyCode == 13) {
                                        this.onGetReciptsByClientId()
                                    }
                                }}
                                value={this.state.accountId}
                                onChange={this.onGetAccountByKey}
                                type="number"
                                placeholder="Enter Account Id"
                                className="form-control"
                            />
                        </FormGroup>
                    </Col>
                    <Col md={3}>
                        <FormGroup>
                            <ControlLabel style={{
                                fontWeight: "bold",
                                fontSize: '18px'
                            }}>ٹوٹل  رقم</ControlLabel>
                            <h4 style={{ fontWeight: "bold", color: 'red' }}>{calCulateTotalReciptAmmount(Data)}</h4>
                        </FormGroup>
                    </Col>


                </Row>
                <ReactTable
                    title={"test"}
                    data={data}
                    filterable
                    columns={Columns}
                    defaultFilterMethod={(filter, row) => filterCaseInsensitive(filter, row)}
                    // manual
                    defaultPageSize={10}
                    // onFetchData={onFetch}
                    showPaginationBottom
                    showPaginationTop={false}
                    // pages={this.state.pages}
                    loading={loading}
                    sortable={false}
                    className="-striped -highlight"
                />

                {
                    this.state.showDeleteModal &&
                    <DeleteAlert
                        hide={() => this.setState({ showDeleteModal: false })}
                        onDelete={() => {
                            this.onConfirmDeleteRecipt()
                        }}
                        text={this.state.selectedObj.clientName.name + " کیاں  آپ  یہ  ڈیلیٹ  کرنا  چھتے  ہے؟"}
                    />
                }

                {showViewModal &&
                    <ViewPrintModal
                        show={showViewModal}
                        hide={() => this.setState({ showViewModal: false })}
                        selectedObj={selectedObj}
                    />
                }
            </div>
        )
    }
}


const mapDispatchToProps = dispatch => {
    return {
        onShowNotification: (errorType, error) =>
            dispatch({
                type: "SET_NOTIFICATION",
                errorType: errorType,
                errorMessage: error
            }),
    }
}
export default connect(null, mapDispatchToProps)(AllRecipt)
