import React, { useState, useEffect } from 'react'
import { Row, Col } from 'react-bootstrap'
import Button from "components/CustomButton/CustomButton.jsx";
import NewAddressModal from 'components/Modals/addNewAddress'
import ReactTable from "react-table";
import { connect } from 'react-redux'
import { allAccounts, deleteAccount } from 'api/api'
import NewAccountModal from 'components/Modals/newAccount'
import { filterCaseInsensitive, calculatePayment } from 'utils/helper'
import UpdateAccountModal from 'components/Modals/updateAccount'
import PayModal from 'components/Modals/payModal'
import DeleteAlert from 'components/Modals/deleteAlert'
import { icons } from 'utils/const'
const Account = (props) => {
    const [showAddressModal, setShowAddressModal] = useState(false)
    const [showNewAccountModal, setShowNewAccounModal] = useState(false)
    const [accounts, setAccounts] = useState([])
    const [selectedObj, setSelectedObj] = useState({})
    const [showUpdateAccountModal, setShowUpdateAccountModal] = useState(false)
    const [showDeleteModal, setShowDeleteModal] = useState(false)
    const [showPayModal, setShowPayModal] = useState(false)
    useEffect(() => {
        onGetAllAccount()
    }, [])
    const onGetAllAccount = async () => {
        try {
            const doc = await allAccounts()
            if (doc.data) {
                setAccounts(doc.data)
            }
        } catch (err) {
            props.onShowNotification("error", err.message)
        }
    }
    const onDeleteAddress = async () => {
        try {
            const doc = await deleteAccount(selectedObj._id, selectedObj.address._id)
            if (doc.data) {
                props.onShowNotification('success', "اکاونٹ  ڈیلیٹ  ھو  چکا  ھیں")
                onGetAllAccount()
            }
        } catch (err) {
            props.onShowNotification("error", err.message)
        }
    }
    const data = accounts.map((prop, sn) => {
        return {
            sn: sn + 1,
            name: prop.name,
            address: prop.address ? prop.address.address : 'error',
            ammount: prop.ammount,
            pay: calculatePayment(prop.pay),
            total: <span style={{ color: 'red' }}> {prop.ammount - calculatePayment(prop.pay)}
            </span>,
            accountLimit: prop.accountLimit,
            accountId: prop.accountId,
            contactNumber: prop.contactNumber,
            actions: <div className="table-action-btn-wrapper">

                <Button onClick={() => {
                    setShowUpdateAccountModal(true)
                    setSelectedObj(prop)
                }} title="Edit" bsStyle="success"
                    bsSize="xs" fill >
                    < i className={icons.edit} />
                </Button>
                <Button onClick={() => {
                    setSelectedObj(prop)
                    setShowDeleteModal(true)
                }} title="Delete" bsStyle="danger"
                    bsSize="xs" fill >
                    < i className={icons.delete} />

                </Button>
                <Button onClick={() => {
                    setSelectedObj(prop)
                    setShowPayModal(true)
                }} title="Payment" bsStyle="info"
                    bsSize="xs" fill >
                    < i className={icons.cart} />

                </Button>
            </div>
        };
    })


    const Columns = [
        {
            Header: "#",
            accessor: "sn",
            filterable: false,
        },
        {
            Header: 'نام',
            accessor: 'name',
            filterable: true,
        },
        {
            Header: 'اکاونٹ  ای  ڈی',
            accessor: 'accountId',
            filterable: true,
        },
        {
            Header: 'گاوں',
            accessor: 'address',
            filterable: true,
        },
        {
            Header: 'موبایل  نمبر',
            accessor: 'contactNumber',
            filterable: true,
        },
        {
            Header: 'رقم',
            accessor: 'ammount',
            filterable: false,
        },
        {
            Header: 'وصولی',
            accessor: 'pay',
            filterable: false,
        },
        {
            Header: 'ٹوٹل  رقم',
            accessor: 'total',
            filterable: false,
        },
        {
            Header: 'خد  ادھار',
            accessor: 'accountLimit',
            filterable: false,
        },

        {

            Header: "اکشن",
            accessor: "actions",
            filterable: false,
        },


    ]
    return (
        <div className="react-table-custom-design">
            <Row className="account-header-btns">
                <Col md={2}>
                    <Button className="btn-submit" fill
                        onClick={() => { setShowNewAccounModal(true) }}>
                        <i className={icons.submit} />
             نیا  کھاتہ
                    </Button>
                </Col>
                <Col md={2}>
                    <Button className="btn-submit" fill
                        onClick={() => {
                            setShowAddressModal(true)
                        }}>
                        <i className={icons.submit} />
                    نیا  گاوں
                    </Button>
                </Col>
            </Row>

            <div className="tbl-header">
                <ReactTable
                    title={"test"}
                    data={data}
                    filterable
                    columns={Columns}
                    defaultFilterMethod={(filter, row) => filterCaseInsensitive(filter, row)}
                    // manual
                    defaultPageSize={10}
                    // onFetchData={onFetch}
                    showPaginationBottom
                    showPaginationTop={false}
                    // pages={this.state.pages}
                    //loading={loading}
                    sortable={false}
                    className="-striped -highlight"
                />
            </div>
            {
                showAddressModal &&
                <NewAddressModal
                    show={showAddressModal}
                    hide={() => setShowAddressModal(false)}
                    onShowNotification={props.onShowNotification}
                    fetchData={() => { }}
                />
            } {
                showNewAccountModal &&
                <NewAccountModal
                    show={showNewAccountModal}
                    hide={() => setShowNewAccounModal(false)}
                    onShowNotification={props.onShowNotification}
                    fetchData={onGetAllAccount}
                />
            }
            {
                showUpdateAccountModal &&
                <UpdateAccountModal
                    show={showUpdateAccountModal}
                    hide={() => setShowUpdateAccountModal(false)}
                    onShowNotification={props.onShowNotification}
                    fetchData={onGetAllAccount}
                    selectedObj={selectedObj}
                />
            }
            {
                showPayModal &&
                <PayModal
                    show={showPayModal}
                    hide={() => setShowPayModal(false)}
                    onShowNotification={props.onShowNotification}
                    fetchData={onGetAllAccount}
                    selectedObj={selectedObj}
                />
            }
            {
                showDeleteModal &&
                <DeleteAlert
                    hide={() => setShowDeleteModal(false)}
                    onDelete={() => {
                        onDeleteAddress()
                    }}
                    text={selectedObj.name + " کیاں  آپ  یہ  ڈیلیٹ  کرنا  چھتے  ہے؟"}
                />
            }

        </div>
    )
}

const mapDispatchToProps = dispatch => {
    return {
        onShowNotification: (errorType, error) =>
            dispatch({
                type: "SET_NOTIFICATION",
                errorType: errorType,
                errorMessage: error
            }),
    }
}
export default connect(null, mapDispatchToProps)(Account)