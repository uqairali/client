import React, { useState, useEffect } from 'react';
import { Col, FormGroup, Row, ControlLabel, Button } from 'react-bootstrap'
import DateTime from "react-datetime";
import Select, { Async } from "react-select";
import { connect } from 'react-redux'
import { allAccounts, getAllReceiptByDateRange } from 'api/api'
import { startAndEndOfTheMonth, calCulateTotalReciptAmmount } from 'utils/helper';
import moment from 'moment';

const Customer = (props) => {
    const [fromMonth, setFromMonth] = useState(new Date())
    const [toMonth, setToMonth] = useState(new Date())
    const [selectedAccount, setSelectedAccount] = useState({})
    const [AllAccounts, setAllAccounts] = useState([])
    const [accountId, setAccountId] = useState("")
    const [totalAmount, setTotalAmount] = useState(0)
    const [data, setData] = useState([])
    useEffect(() => {
        onGetAllAccount()
    }, [])

    const onGetAllAccount = async () => {
        try {
            const doc = await allAccounts()
            if (doc.data) {
                const mapData = doc.data.map(itm => {
                    return {
                        label: itm.name + "(" + itm.address.address + ")",
                        value: itm.name + "(" + itm.address.address + ")",
                        data: itm
                    }
                })
                setAllAccounts(mapData)
            }
        } catch (err) {
            props.onShowNotification("error", err.message)
        }
    }
    const onGetAccountByKey = async (e) => {
        try {
            setAccountId(e.target.value)
            const filterData = AllAccounts.filter(itm => itm.data.accountId === e.target.value)
            if (filterData.length) {
                setSelectedAccount({
                    label: filterData[0].label,
                    value: filterData[0].label,
                    data: filterData[0].data
                })
            } else {
                setSelectedAccount({})
            }
        } catch (err) {
            props.onShowNotification("error", err.message)
        }
    }

    const handleSearch = async () => {
        try {
            const reqBody = {
                startDate: moment(startAndEndOfTheMonth(fromMonth).startOfMonth).format("MM-DD-YYYY"),
                endDate: moment(startAndEndOfTheMonth(toMonth).endOfMonth).format("MM-DD-YYYY"),
                userId: selectedAccount.data ? selectedAccount.data._id : ""
            }

            const result = await getAllReceiptByDateRange(reqBody)
            setData(result.data)
            setTotalAmount(calCulateTotalReciptAmmount(result.data))

        } catch (err) {
            props.onShowNotification("error", err.message)
        }
    }
    return (
        <div className="react-table-custom-design">

            <Row>
                <Col md={2}>
                    <FormGroup>
                        <ControlLabel style={{
                            fontWeight: "bold",
                            fontSize: '18px'
                        }}>مہینے سے</ControlLabel>
                        <DateTime
                            dateFormat="MMM-YYYY" timeFormat={false}
                            inputProps={{ placeholder: "Select date" }}
                            defaultValue={new Date()}
                            onChange={(d) => setFromMonth(d)}
                            value={fromMonth}
                            closeOnSelect
                        />
                    </FormGroup>
                </Col>

                <Col md={2}>
                    <FormGroup>
                        <ControlLabel style={{
                            fontWeight: "bold",
                            fontSize: '18px'
                        }}>مہینے تک </ControlLabel>
                        <DateTime
                            dateFormat="MMM-YYYY" timeFormat={false}
                            inputProps={{ placeholder: "Select date" }}
                            defaultValue={new Date()}
                            onChange={(d) => setToMonth(d)}
                            value={toMonth}
                            closeOnSelect
                        />
                    </FormGroup>
                </Col>

                <Col md={2}>
                    <FormGroup >
                        <ControlLabel style={{
                            fontWeight: "bold",
                            fontSize: '18px'
                        }}>نام  منتخب  کریں </ControlLabel>
                        <Select
                            clearable={false}
                            placeholder={"Single Select"}
                            value={selectedAccount}
                            options={AllAccounts}
                            onChange={data => {
                                if (data) {
                                    setAccountId(data.data.accountId)
                                    setSelectedAccount(data)
                                }
                            }}
                        />
                    </FormGroup>
                </Col>
                <Col md={2}>
                    <FormGroup >
                        <ControlLabel style={{
                            fontWeight: "bold",
                            fontSize: '18px'
                        }}>اکاونٹ  ای  ڈی  ڈالیں</ControlLabel>
                        <input
                            onKeyPress={event => {
                                if (event.which == 13 || event.keyCode == 13) {
                                    //get data fom api
                                }
                            }}
                            value={accountId}
                            onChange={onGetAccountByKey}
                            type="number"
                            placeholder="Enter Account Id"
                            className="form-control"
                        />
                    </FormGroup>
                </Col>
           

                <Col md={2}>
                    <Button onClick={handleSearch} style={{ marginTop: "12%" }} className="btn-submit" fill>Search</Button>
                </Col>
               

                <Col md={2}>
                        <FormGroup>
                            <ControlLabel style={{
                                fontWeight: "bold",
                                fontSize: '18px'
                            }}>ٹوٹل  رقم</ControlLabel>
                            <h4 style={{ fontWeight: "bold", color: 'red' }}>{totalAmount}</h4>
                        </FormGroup>
                    </Col>
            </Row>
        </div>
    )
}


const mapDispatchToProps = dispatch => {
    return {
        onShowNotification: (errorType, error) =>
            dispatch({
                type: "SET_NOTIFICATION",
                errorType: errorType,
                errorMessage: error
            }),
    }
}

export default connect(null, mapDispatchToProps)(Customer)

