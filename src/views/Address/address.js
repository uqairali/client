import React, { useState, useEffect } from 'react'
import { Grid, Row, Col } from 'react-bootstrap'
import { alladdreses,deleteAddress } from '../../api/api'
import Button from "components/CustomButton/CustomButton.jsx";
import ReactTable from "react-table";
import { filterCaseInsensitive,calCulateAmmountByAddress } from 'utils/helper'
import { connect } from 'react-redux'
import NewAddressModal from 'components/Modals/addNewAddress'
import EditAddressModal from 'components/Modals/editAddress'
import DeleteAlert from 'components/Modals/deleteAlert'
import SwitchAddresesModal from 'components/Modals/switchAddreses'
import { icons } from 'utils/const'
const AllAddreses = (props) => {
    const [Data, setData] = useState([])
    const [selectedObj, setSelectedObj] = useState({})
    const [showEditModal, setShowEditModal] = useState(false)
    const [showDeleteModal, setShowDeleteModal] = useState(false)
    const [showAddressModal, setShowAddressModal] = useState(false)
    const [showSwitchAddressModal,setShowSwitchAddressModal]=useState(false)
    useEffect(() => {
        onGetAllAddreses()
    }, [])
    const onGetAllAddreses = async () => {
        try {
            const data = await alladdreses()
            setData(data.data)
        } catch (err) {
            props.onShowNotification('error', err.message)
        }
    }
    const onDeleteAddress=async()=>{
        try{
              const doc=await deleteAddress(selectedObj._id)
              if(doc.data){
                  props.onShowNotification('success',"گاوں  ڈیلیٹ  ھو  چکا  ھیں")
                  onGetAllAddreses()
              }
        }catch(err){
            props.onShowNotification("error",err.message)
        }
    }
    const data = Data.map((prop, sn) => {
        return {
            sn: sn + 1,
            address: prop.address,
            people: prop.names.length,
            ammount: calCulateAmmountByAddress(prop.names),
            actions: <div className="table-action-btn-wrapper">

                <Button onClick={() => {
                    setSelectedObj(prop)
                    setShowEditModal(true)
                }} title="Edit" bsStyle="success"
                    bsSize="xs" fill >
                    < i className={icons.edit} />
                </Button>
                <Button onClick={() => {
                    setSelectedObj(prop)
                    setShowDeleteModal(true)
                }} title="Delete" bsStyle="danger"
                    bsSize="xs" fill >
                    < i className={icons.delete} />

                </Button>
            </div>
        };
    })


    const Columns = [
        {
            Header: "#",
            accessor: "sn",
            filterable: false,
        },
        {
            Header: 'گاوں',
            accessor: 'address',
            filterable: true,
        },
        {
            Header: 'لوگ',
            accessor: 'people',
            filterable: false,
        },
        {
            Header: 'روپے',
            accessor: 'ammount',
            filterable: false,
        },

        {

            Header: "اکشن",
            accessor: "actions",
            filterable: false,
        },


    ]
    return (
        <Grid fluid className="react-table-custom-design">
            <Row>
                <Col md={2}>
                    <Button className="btn-submit" fill
                        onClick={() => {
                            setShowAddressModal(true)
                        }}>
                        <i className={icons.submit} />
                    نیا  گاوں
                    </Button>
                </Col>
            </Row>
            <div className="tbl-header">
                <ReactTable
                    title={"test"}
                    data={data}
                    filterable
                    columns={Columns}
                    defaultFilterMethod={(filter, row) => filterCaseInsensitive(filter, row)}
                    // manual
                    defaultPageSize={10}
                    // onFetchData={onFetch}
                    showPaginationBottom
                    showPaginationTop={false}
                    // pages={this.state.pages}
                    //loading={loading}
                    sortable={false}
                    className="-striped -highlight"
                />
            </div>

            {
                showAddressModal &&
                <NewAddressModal
                    show={showAddressModal}
                    hide={() => setShowAddressModal(false)}
                    onShowNotification={props.onShowNotification}
                    fetchData={onGetAllAddreses}
                />
            }
            {
                showEditModal &&
                <EditAddressModal
                    show={showEditModal}
                    hide={() => setShowEditModal(false)}
                    onShowNotification={props.onShowNotification}
                    selectedObj={selectedObj}
                    fetchData={onGetAllAddreses}
                />
            }
             {
                    showDeleteModal &&
                    <DeleteAlert
                        hide={() =>setShowDeleteModal(false)}
                        onDelete={()=>{
                            if(selectedObj.names.length)
                            setShowSwitchAddressModal(true)
                            else
                            onDeleteAddress()
                        }}
                        text={selectedObj.address+" کیاں  آپ  یہ  ڈیلیٹ  کرنا  چھتے  ہے؟"}
                    />
                }
                {
                    showSwitchAddressModal&&
                    <SwitchAddresesModal
                    show={showSwitchAddressModal}
                    hide={() => setShowSwitchAddressModal(false)}
                    onShowNotification={props.onShowNotification}
                    selectedObj={selectedObj}
                    Data={Data}
                    onDeleteAddress={onDeleteAddress}
                    />
                }
        </Grid>
    )
}
const mapDispatchToProps = dispatch => {
    return {
        onShowNotification: (errorType, error) =>
            dispatch({
                type: "SET_NOTIFICATION",
                errorType: errorType,
                errorMessage: error
            }),
    }
}
export default connect(null, mapDispatchToProps)(AllAddreses)