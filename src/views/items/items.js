import React, { useState, useEffect } from 'react'
import { Row, Col } from 'react-bootstrap'
import Button from "components/CustomButton/CustomButton.jsx";
import ReactTable from "react-table";
import AddItemModal from 'components/Modals/addItems'
import { connect } from 'react-redux'
import { getAllItems, deleteItem } from 'api/api'
import { filterCaseInsensitive } from 'utils/helper'
import EditItemModal from 'components/Modals/editItem'
import DeleteAlert from 'components/Modals/deleteAlert'
import { icons } from 'utils/const'
const ItemsManagement = (props) => {
    const [showNewItemModal, setShowNewItemModal] = useState(false)
    const [Data, setData] = useState([])
    const [selectedObj, setSelectedObj] = useState({})
    const [showEditItemModal, setShowItemModal] = useState(false)
    const [showDeleteModal, setShowDeleteModal] = useState(false)

    useEffect(() => {
        onGetAllItems()
    }, [])
    const onGetAllItems = async () => {
        try {
            const doc = await getAllItems()
            if (doc.data) {
                setData(doc.data)
            }
        } catch (err) {
            props.onShowNotification("error", err.message)
        }
    }
    const onDeleteAddress = async () => {
        try {
            const doc = await deleteItem(selectedObj._id)
            if (doc.data) {
                props.onShowNotification('success', "ایٹم  ڈیلیٹ  ھو  چکا  ھیں")
                onGetAllItems()
            }
        } catch (err) {
            props.onShowNotification("error", err.message)
        }
    }
    const data = Data.map((prop, sn) => {
        return {
            sn: sn + 1,
            name: prop.name,
            price: prop.price,
            count: prop.count,
            shortKey: prop.shortKey,
            actions: <div className="table-action-btn-wrapper">

                <Button onClick={() => {
                    setSelectedObj(prop)
                    setShowItemModal(true)
                }} title="Edit" bsStyle="success"
                    bsSize="xs" fill >
                    < i className={icons.edit} />
                </Button>
                <Button onClick={() => {
                    setSelectedObj(prop)
                    setShowDeleteModal(true)
                }} title="Delete" bsStyle="danger"
                    bsSize="xs" fill >
                    < i className={icons.delete} />

                </Button>
            </div>
        };
    })


    const Columns = [
        {
            Header: "#",
            accessor: "sn",
            filterable: false,
        },
        {
            Header: 'نام',
            accessor: 'name',
            filterable: true,
        },
        {
            Header: 'روپے',
            accessor: 'price',
            filterable: false,
        },
        {
            Header: 'ٹوٹل  تعداد',
            accessor: 'count',
            filterable: false,
        },
        {
            Header: 'شارٹ  کی',
            accessor: 'shortKey',
            filterable: true,
        },

        {

            Header: "اکشن",
            accessor: "actions",
            filterable: false,
        },


    ]
    return (
        <div className="react-table-custom-design">
            <Row className="urdu-btn-text">
                <Col md={2}>
                    <Button className="btn-submit" fill
                        onClick={() => {
                            setShowNewItemModal(true)
                        }}>
                        <i className={icons.submit} />
                    نیا  ایٹم
                    </Button>
                </Col>
            </Row>
            <div className="tbl-header">
                <ReactTable
                    title={"test"}
                    data={data}
                    filterable
                    columns={Columns}
                    defaultFilterMethod={(filter, row) => filterCaseInsensitive(filter, row)}
                    // manual
                    defaultPageSize={10}
                    // onFetchData={onFetch}
                    showPaginationBottom
                    showPaginationTop={false}
                    // pages={this.state.pages}
                    //loading={loading}
                    sortable={false}
                    className="-striped -highlight"
                />
            </div>
            {showNewItemModal &&
                <AddItemModal
                    show={showNewItemModal}
                    hide={() => setShowNewItemModal(false)}
                    onShowNotification={props.onShowNotification}
                    fetchData={onGetAllItems}
                />
            }
            {showEditItemModal &&
                <EditItemModal
                    show={showEditItemModal}
                    hide={() => setShowItemModal(false)}
                    onShowNotification={props.onShowNotification}
                    fetchData={onGetAllItems}
                    selectedObj={selectedObj}
                />
            }
            {
                showDeleteModal &&
                <DeleteAlert
                    hide={() => setShowDeleteModal(false)}
                    onDelete={() => {

                        onDeleteAddress()
                    }}
                    text={selectedObj.name + " کیاں  آپ  یہ  ڈیلیٹ  کرنا  چھتے  ہے؟"}
                />
            }
        </div>
    )
}
const mapDispatchToProps = dispatch => {
    return {
        onShowNotification: (errorType, error) =>
            dispatch({
                type: "SET_NOTIFICATION",
                errorType: errorType,
                errorMessage: error
            }),
    }
}
export default connect(null, mapDispatchToProps)(ItemsManagement)