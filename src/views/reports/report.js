import React, { useState, useEffect, Fragment } from 'react'
import { alladdreses } from 'api/api'
import { connect } from 'react-redux'
import { Row, Col, Grid, Table } from 'react-bootstrap'
import Select from "react-select";
import Card from "components/Card/Card.jsx";
import { jsPDF } from "jspdf";
import html2canvas from 'html2canvas'
import { formatDate } from 'utils/helper'
import Button from "components/CustomButton/CustomButton.jsx";
const Reports = ({ onShowNotification }) => {
    const [allAddresses, setAllAddresses] = useState([])
    const [selectedAddress, setSelectedAddress] = useState([])
    const [pdfLoading,setPdfLoading]=useState(false)
    useEffect(() => {
        onGetAllAddresses()
    }, [])
    const onGetAllAddresses = async () => {
        try {
            const data = await alladdreses()
            if (data.data) {
                const mapData = data.data.map(itm => {
                    return { label: itm.address, value: itm.address, data: itm }
                })
                mapData.unshift({ label: 'All', value: 'All' })
                setAllAddresses(mapData)
            }
        } catch (err) {
            onShowNotification('error', err.message)
        }
    }
    const printDocument = () => {
        setPdfLoading(true)
        const input = document.getElementById('divToPrint');
        html2canvas(input)
            .then((canvas) => {
                const imgData = canvas.toDataURL('image/png');
                var imgWidth = 210;
                var pageHeight = 295;
                var imgHeight = canvas.height * imgWidth / canvas.width;
                var heightLeft = imgHeight;
                var doc = new jsPDF('p', 'mm');
                var position = 10; // give some top padding to first page

                doc.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight);
                heightLeft -= pageHeight;

                while (heightLeft >= 0) {
                    position += heightLeft - imgHeight; // top padding for other pages
                    doc.addPage();
                    doc.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight);
                    heightLeft -= pageHeight;
                }
                doc.save(formatDate(new Date()) + '.pdf');
                setPdfLoading(false)
            })
            ;
    }
    console.log(selectedAddress)
    return (
        <Grid fluid>
            <Row>

                <Col md={8}>
                    <Select
                        value={selectedAddress}
                        options={allAddresses}
                        isMulti={true}
                        onChange={data => {
                            if (data) {
                                var found = data.find((post, index) => {
                                    if (post.value === 'All')
                                        return true;
                                });

                                if (found) {
                                    var filterResult = allAddresses.filter(itm => {
                                        return itm.value !== "All"
                                    })
                                    setSelectedAddress(filterResult)
                                }
                                else
                                    setSelectedAddress(data)
                            }
                        }}
                    />
                </Col>
                <Col md={4}>
                    <Button disabled={!selectedAddress.length||pdfLoading} onClick={printDocument} title="Print" bsStyle="primary" fill >
                        Print Table
                    </Button>
                </Col>
            </Row>
            <Row>
                <Col md={12}>
                    <Card
                        title={'Report'}
                        ctAllIcons
                        content={
                            <div className="recipt-show" id="divToPrint">
                                {
                                    <Table responsive>
                                        <thead>

                                            <tr className="user-details-header">
                                                <th>Name</th>
                                                <th>ID</th>
                                                <th>Address</th>
                                                <th>Recipts</th>
                                                <th>Mobile Number</th>
                                                <th>Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <Fragment>
                                                {
                                                    selectedAddress.map((itm, key) => (
                                                        <Fragment key={'parant' + key}>
                                                            {
                                                                itm.data.names.map((nm, key) => (
                                                                    <tr key={key}>
                                                                        <th>{nm.name}</th>
                                                                        <th>{nm.accountId}</th>
                                                                        <th>{itm.value}</th>
                                                                        <th>{nm.recipts.length}</th>
                                                                        <th>{nm.contactNumber}</th>
                                                                        <th>{nm.ammount}</th>
                                                                    </tr>
                                                                ))
                                                            }
                                                        </Fragment>

                                                    ))



                                                }

                                            </Fragment>
                                        </tbody>
                                    </Table>

                                }
                            </div>
                        }
                    />
                </Col>
            </Row>
        </Grid>
    )
}

const mapDispatchToProps = dispatch => {
    return {
        onShowNotification: (errorType, error) =>
            dispatch({
                type: "SET_NOTIFICATION",
                errorType: errorType,
                errorMessage: error
            }),
    }
}
export default connect(null, mapDispatchToProps)(Reports)