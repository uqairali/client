/*!

=========================================================
* Light Bootstrap Dashboard React - v1.3.0
=========================================================

* Product Page: https://www.creative-tim.com/product/light-bootstrap-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/light-bootstrap-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/

import {
  Grid,
  Col,
  FormGroup,
  ControlLabel,
  Row,
  Form
} from "react-bootstrap";

import Card from "components/Card/Card.jsx";

import Button from "components/CustomButton/CustomButton.jsx";
import { Redirect } from "react-router-dom";
import React, { useState } from "react";
import { useAuth } from 'context/auth';

import { login } from '../api/api';
import useForm from "react-hook-form";
import Checkbox from "components/CustomCheckbox/CustomCheckbox.jsx";
function Login(props) {

  const { setAuthTokens, authTokens } = useAuth();
  const [serverError, setServerError] = useState();
  const [loading, setLoading] = useState(false);
  const { handleSubmit, register, errors } = useForm();
  const [OnShowPassword,setOnShowPassword]=useState(false)
  const onSubmit = async (values) => {
    try{
    setServerError(null);
   setLoading(true)
   let response = await login(values.email, values.password);
    setLoading(false)
    if(response.data)
    setAuthTokens({ access_token: response.data.user.token });
    
  }
  catch(err){
    setLoading(false)
    setServerError(err.message);
  }

  }

  console.log(authTokens)
  return (
    authTokens && authTokens.access_token ? <Redirect to="/" /> :
      <div className="login-wrapper">
        <section className="about">
        </section>
        <Grid fluid>
         
          <Row>
            <Col md={4} sm={6} mdOffset={4} smOffset={3}>
              <Card
                title="Log In"
                ctAllIcons
                content={
                  <Form onSubmit={handleSubmit(onSubmit)}>

                    <small className="text-danger">{serverError || null}</small>
                    <FormGroup
                      validationState={errors.email && errors.email.message ? "error" : "success"}
                    >
                      <ControlLabel>Email address</ControlLabel>
                      <input
                        name="email"
                        ref={register({
                          required: 'Required',
                          pattern: {
                            value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                            message: "invalid email address"
                          }
                        })}
                        placeholder="Enter your email"
                        className="form-control"
                      />

                      {(errors.email && errors.email.message) && <small className="text-danger">{errors.email && errors.email.message}</small>}


                    </FormGroup>
                    <FormGroup
                      validationState={errors.password && errors.password.message ? "error" : "success"}
                    >
                      <ControlLabel>Password</ControlLabel>
                      <input
                        name="password"
                        type={OnShowPassword?"text":'password'}
                        ref={register({
                          required: 'Required',

                        })}
                        placeholder="Enter your password"
                        className="form-control"
                      />
                      {(errors.password && errors.password.message) && <small className="text-danger">{errors.password && errors.password.message}</small>}

                    </FormGroup>
                    <FormGroup style={{color:'white'}} >
                    <Checkbox
                            checked={OnShowPassword}
                            number="1"
                            label="Show Password"
                            onClick={()=>setOnShowPassword(!OnShowPassword)}
                          />
                    </FormGroup>
                    <div className="center">
                      <Button disabled={loading} className="btn-submit" fill type="submit"><i className={loading?"fa fa-spin fa-spinner":"fa fa-lock"}/>SIGN IN</Button>
                    </div>
                  </Form>


                } />

            </Col>

          </Row>
        </Grid>
      </div >
  );

}

export default Login;
