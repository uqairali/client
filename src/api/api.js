import axios from 'axios';
import resolve from './resolve';

  let apiBase = process.env.REACT_APP_API_BASEURL;
let apiBaseUrl=apiBase+"/api"

const login = async (email, password) => {
    return await resolve(axios.post(`${apiBaseUrl}/auth/login`, {
        email,
        password
    }).then(res => res.data));
}

const newAddress = async (data) =>{
    return await resolve(axios.post(`${apiBaseUrl}/newAddress`,data).then(res => res.data));
}
const alladdreses = async () =>{
    return await resolve(axios.get(`${apiBaseUrl}/allAddreses`).then(res => res.data));
}
const editAddress = async (id,data) =>{
    return await resolve(axios.put(`${apiBaseUrl}/editAddress/${id}`,data).then(res => res.data));
}
const deleteAddress = async (id) =>{
    return await resolve(axios.delete(`${apiBaseUrl}/deleteAddress/${id}`).then(res => res.data));
}
const getAccountId = async () =>{
    return await resolve(axios.get(`${apiBaseUrl}/getAccountId/`).then(res => res.data));
}
const newAccount = async (data) =>{
    return await resolve(axios.post(`${apiBaseUrl}/newAccount`,data).then(res => res.data));
}
const allAccounts = async () =>{
    return await resolve(axios.get(`${apiBaseUrl}/getAllAccounts`).then(res => res.data));
}
const incrementAccountId = async (id) =>{
    return await resolve(axios.get(`${apiBaseUrl}/incrementAccountId/${id}`).then(res => res.data));
}
const updateAccount = async (id,previusAddress,data) =>{
    return await resolve(axios.put(`${apiBaseUrl}/updateAccounts/${id}/${previusAddress}`,data).then(res => res.data));
}
const deleteAccount = async (id,addressId) =>{
    return await resolve(axios.delete(`${apiBaseUrl}/deleteAccounts/${id}/${addressId}`).then(res => res.data));
}
const addNewItem = async (data) =>{
    return await resolve(axios.post(`${apiBaseUrl}/newItem`,data).then(res => res.data));
}
const getAllItems = async () =>{
    return await resolve(axios.get(`${apiBaseUrl}/getAllItems`).then(res => res.data));
}
const editItem = async (id,data) =>{
    return await resolve(axios.put(`${apiBaseUrl}/updateItem/${id}`,data).then(res => res.data));
}
const deleteItem = async (id) =>{
    return await resolve(axios.delete(`${apiBaseUrl}/deleteItem/${id}`).then(res => res.data));
}
const getAccountByKey = async (key) =>{
    return await resolve(axios.get(`${apiBaseUrl}/getAccountByKey/${key}`).then(res => res.data));
}
const getAccountById = async (id) =>{
    return await resolve(axios.get(`${apiBaseUrl}/getAccountById/${id}`).then(res => res.data));
}
const getReciptSN = async () =>{
    return await resolve(axios.get(`${apiBaseUrl}/getReciptSerialNumber`).then(res => res.data));
}
const UpdateReciptSN = async (id) =>{
    return await resolve(axios.put(`${apiBaseUrl}/updateReciptSN/${id}`).then(res => res.data));
}
const newRecipt = async (date,data) =>{
    return await resolve(axios.post(`${apiBaseUrl}/newRecipt/${date}`,data).then(res => res.data));
}
const getAllRecipt = async (date) =>{
    return await resolve(axios.get(`${apiBaseUrl}/getReciptByDate/${date}`).then(res => res.data));
}
const getReciptByName = async (name) =>{
    return await resolve(axios.get(`${apiBaseUrl}/getReciptByName/${name}`).then(res => res.data));
}
const deleteRecipt = async (reciptID,accountId,total) =>{
    return await resolve(axios.delete(`${apiBaseUrl}/deleteRecipt/${reciptID}/${accountId}/${total}`).then(res => res.data));
}
const getReciptByID = async (id) =>{
    return await resolve(axios.get(`${apiBaseUrl}/getReciptById/${id}`).then(res => res.data));
}
const onUpdateRecipt = async (id,data,date,total) =>{
    return await resolve(axios.put(`${apiBaseUrl}/updateRecipt/${id}/${date}/${total}`,data).then(res => res.data));
}
const newPay = async (data,date) =>{
    return await resolve(axios.post(`${apiBaseUrl}/newPay/${date}`,data).then(res => res.data));
}
const allPays = async () =>{
    return await resolve(axios.get(`${apiBaseUrl}/payes/`).then(res => res.data));
}
const getPayByName = async (name) =>{
    return await resolve(axios.get(`${apiBaseUrl}/getPayesByName/${name}`).then(res => res.data));
}
const allResports = async () =>{
    return await resolve(axios.get(`${apiBaseUrl}/allReport`).then(res => res.data));
}
const updatePay = async (id,data,date) =>{
    return await resolve(axios.put(`${apiBaseUrl}/updatePay/${id}/${date}`,data).then(res => res.data));
}

const deletePys = async (id) =>{
    return await resolve(axios.delete(`${apiBaseUrl}/deletePays/${id}`).then(res => res.data));
}

const getAllReceiptByDateRange = async (data) =>{
    return await resolve(axios.post(`${apiBaseUrl}/getReciptByDateRange`,data).then(res => res.data));
}

export {
    login,
    newAddress,
    alladdreses,
    editAddress,
    deleteAddress,
    getAccountId,
    newAccount,
    allAccounts,
    incrementAccountId,
    updateAccount,
    deleteAccount,
    addNewItem,
    getAllItems,
    editItem,
    deleteItem,
    getAccountByKey,
    getAccountById,
    getReciptSN,
    UpdateReciptSN,
    newRecipt,
    getAllRecipt,
    getReciptByName,
    deleteRecipt,
    getReciptByID,
    onUpdateRecipt,
    newPay,
    allPays,
    getPayByName,
    allResports,
    updatePay,
    deletePys,
    getAllReceiptByDateRange
}
